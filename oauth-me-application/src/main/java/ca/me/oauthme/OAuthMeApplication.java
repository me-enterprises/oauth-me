package ca.me.oauthme;

import ca.me.oauthme.core.util.SanitizerUtil;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

/**
 * Spring application initializer.
 *
 * @author Rodrigo da Silva
 * @version 1.0.0
 * @since 1.0.0
 */
@EnableCaching
@SpringBootApplication
public class OAuthMeApplication {
    public static void main(String[] args) {
        SpringApplication.run(OAuthMeApplication.class, SanitizerUtil.sanitize(args));
    }
}
