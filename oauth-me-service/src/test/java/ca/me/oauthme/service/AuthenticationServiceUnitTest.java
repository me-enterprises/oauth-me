package ca.me.oauthme.service;

import ca.me.oauthme.AbstractUnitTest;
import ca.me.oauthme.core.util.JwtUtil;
import ca.me.oauthme.model.db.ModelTestHelper;
import ca.me.oauthme.repository.RoleResourceScopeMappingRepository;
import ca.me.oauthme.repository.UserRepository;
import ca.me.oauthme.service.dto.AuthenticationRequestDto;
import ca.me.oauthme.service.exception.InvalidPasswordException;
import ca.me.oauthme.service.exception.UnexpectedErrorException;
import ca.me.oauthme.service.exception.UserNotFoundException;
import ca.me.oauthme.service.impl.AuthenticationServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.context.MessageSource;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.Collections;
import java.util.Objects;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Unit test class to test {@link AuthenticationServiceImpl} class
 *
 * @author Rodrigo da Silva
 * @version 1.0.0
 * @since 1.0.0
 */
class AuthenticationServiceUnitTest extends AbstractUnitTest {

    private static final int jwtValidity = 5 * 60 * 1_000; // 5 minutes.
    private static final String USERNAME = "admin@email.com";
    private static final String PASSWORD = "123456789";
    private static final String UNIQUE_ID = "c8afb570-dd5d-4c49-8bcd-0af9b47dc9d0'";
    private static final String ROLE_NAME = "ADMIN";
    private static final String RESOURCE_NAME = "Resource";
    private static final String RESOURCE_PATH = "/path/to/resource";
    private static final String SCOPE_NAME = "RESOURCE_READ";
    private final JwtUtil jwtUtil = new JwtUtil();
    private final PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    @Mock
    private UserRepository userRepository;

    @Mock
    private RoleResourceScopeMappingRepository mappingRepository;

    @Mock
    private MessageSource messages;

    private AuthenticationService authService;

    @BeforeEach
    void setUp() {
        // Injects jwtSecretKey and jwtValidity in seconds manually.
        setField("jwtSecretKey", "jwtSecretKeyForTesting!@#$%^&*()_+", jwtUtil);
        setField("jwtValidity", jwtValidity, jwtUtil);

        if (Objects.isNull(authService)) {
            authService = new AuthenticationServiceImpl(passwordEncoder, userRepository, mappingRepository, messages, jwtUtil);
        }

        setField("refreshTokenSize", 128, authService);
        setField("refreshTokenValidity", 3600, authService);
    }

    @Test
    void testAuthenticate_whenUserDoesNotExist_throwsUserNotFoundException() {
        final var authRequest = AuthenticationRequestDto.builder().username(USERNAME).password(PASSWORD).build();
        when(userRepository.findByUsername(eq(USERNAME)))
            .thenReturn(Optional.empty());
        assertThrows(UserNotFoundException.class, () -> authService.login(authRequest));
    }

    @Test
    void testAuthenticate_whenPasswordDoesNotMatch_throwsInvalidPasswordException() {
        final var authRequest = AuthenticationRequestDto.builder().username(USERNAME).password("INVALID_PASSWORD").build();
        when(userRepository.findByUsername(eq(USERNAME)))
            .thenReturn(Optional.of(ModelTestHelper.newUser(USERNAME, PASSWORD, UNIQUE_ID)));
        assertThrows(InvalidPasswordException.class, () -> authService.login(authRequest));
    }

    @Test
    void testAuthenticate_whenUnexpectedErrorHappens_throwsUnexpectedErrorException() {
        final var authRequest = AuthenticationRequestDto.builder().username(USERNAME).password("INVALID_PASSWORD").build();
        when(userRepository.findByUsername(eq(USERNAME)))
            .thenThrow(new RuntimeException("Unexpected Error!"));
        assertThrows(UnexpectedErrorException.class, () -> authService.login(authRequest));
    }

    @Test
    void testAuthenticate_whenSuccess_withUserWithZeroScopes() throws UserNotFoundException, InvalidPasswordException, UnexpectedErrorException {
        final var authRequest = AuthenticationRequestDto.builder().username(USERNAME).password(PASSWORD).build();
        final var user = ModelTestHelper.newUser(USERNAME, PASSWORD, UNIQUE_ID);
        when(userRepository.findByUsername(eq(USERNAME)))
            .thenReturn(Optional.of(user));
        when(userRepository.save(eq(user)))
            .thenReturn(user);
        when(mappingRepository.findByUser(eq(user)))
            .thenReturn(Collections.emptySet());
        final var authResponse = authService.login(authRequest);
        assertNotNull(authResponse);
        verify(userRepository, times(1)).save(user);
        final var scopes = jwtUtil.getJwtScope(authResponse.getAccessToken());
        assertTrue(scopes.isEmpty());
        assertTrue(jwtUtil.isJwtValid(authResponse.getAccessToken(), user.getUsername(), user.getUniqueId()));
        assertTrue(StringUtils.isNotBlank(authResponse.getRefreshToken()));
    }

    @Test
    void testAuthenticate_whenSuccess_withUserWithNonZeroScopes() throws UserNotFoundException, InvalidPasswordException, UnexpectedErrorException {
        final var authRequest = AuthenticationRequestDto.builder().username(USERNAME).password(PASSWORD).build();
        final var user = ModelTestHelper.newUser(USERNAME, PASSWORD, UNIQUE_ID);
        when(userRepository.findByUsername(eq(USERNAME)))
            .thenReturn(Optional.of(user));
        when(userRepository.save(eq(user)))
            .thenReturn(user);
        when(mappingRepository.findByUser(eq(user)))
            .thenReturn(
                Collections.singleton(
                    ModelTestHelper.newMapping(
                        ModelTestHelper.newRole(ROLE_NAME),
                        ModelTestHelper.newResource(RESOURCE_NAME, RESOURCE_PATH),
                        ModelTestHelper.newScope(SCOPE_NAME))));
        final var authResponse = authService.login(authRequest);
        assertNotNull(authResponse);
        verify(userRepository, times(1)).save(user);
        final var scopes = jwtUtil.getJwtScope(authResponse.getAccessToken());
        assertEquals(1, scopes.size());
        assertTrue(scopes.contains(RESOURCE_NAME + "::" + SCOPE_NAME));
        assertTrue(jwtUtil.isJwtValid(authResponse.getAccessToken(), user.getUsername(), user.getUniqueId()));
        assertTrue(StringUtils.isNotBlank(authResponse.getRefreshToken()));
    }

    @Test
    void testLogout_whenSuccess() {
        assertTrue(authService.logout(USERNAME));
    }

    @Test
    void testGetUsernameFromToken_whenTokenIsNull_returningEmpty() {
        assertEquals(StringUtils.EMPTY, authService.getUsernameFromToken(null));
    }

    @Test
    void testGetUsernameFromToken_whenTokenIsEmpty_returningEmpty() {
        assertEquals(StringUtils.EMPTY, authService.getUsernameFromToken(StringUtils.EMPTY));
    }

    @Test
    void testGetUsernameFromToken_whenTokenIsCorrect_returningUsername() {
        final var subject = "SUBJECT";
        final var audience = "AUDIENCE";
        final var scopes = Collections.singletonList("isLoggedIn::read");
        ReflectionTestUtils.setField(jwtUtil, "jwtSecretKey", "JwtTestSecret");
        ReflectionTestUtils.setField(jwtUtil, "jwtValidity", 60000);
        assertEquals(subject, authService.getUsernameFromToken(jwtUtil.generateToken(subject, audience, scopes)));
    }
}
