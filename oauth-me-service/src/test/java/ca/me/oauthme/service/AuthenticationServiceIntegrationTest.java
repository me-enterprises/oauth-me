package ca.me.oauthme.service;

import ca.me.oauthme.AbstractIntegrationTest;
import ca.me.oauthme.TestRedisConfiguration;
import ca.me.oauthme.service.dto.AuthenticationRequestDto;
import ca.me.oauthme.service.exception.TokenNotFoundException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.test.context.ContextConfiguration;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Integration Test class for {@link AuthenticationService} class
 *
 * @author Rodrigo da Silva
 * @version 1.0.0
 * @see AuthenticationService
 * @since 1.0.0
 */
@EnableCaching
@ContextConfiguration(classes = {TestRedisConfiguration.class})
class AuthenticationServiceIntegrationTest extends AbstractIntegrationTest {

    private static final String VALID_USERNAME = "admin@email.com";
    private static final String INVALID_USERNAME = "INVALID_USERNAME";
    private static final String PASSWORD = "123456789";

    @Autowired
    private AuthenticationService authService;

    @Test
    void testRetrieveTokenFromCache_whenSuccess() throws Exception {
        final var authRequest = AuthenticationRequestDto.builder().username(VALID_USERNAME).password(PASSWORD).build();
        final var authToken1 = authService.login(authRequest);
        final var cachedToken = authService.retrieveTokens(VALID_USERNAME);
        final var authToken2 = authService.login(authRequest);
        assertEquals(authToken1, cachedToken);
        assertNotEquals(authToken1, authToken2);
    }

    @Test
    void testRetrieveTokenFromCache_whenTokenNotFound() {
        assertThrows(TokenNotFoundException.class, () -> authService.retrieveTokens(INVALID_USERNAME));
    }

    @Test
    void testRetrieveTokenFromCache_whenUsernameIsNull() {
        assertThrows(IllegalArgumentException.class, () -> authService.retrieveTokens(null));
    }

    @Test
    void testLogout_whenSuccess() throws Exception {
        final var authRequest = AuthenticationRequestDto.builder().username(VALID_USERNAME).password(PASSWORD).build();
        assertNotNull(authService.login(authRequest));
        assertNotNull(authService.retrieveTokens(VALID_USERNAME));
        assertTrue(authService.logout(VALID_USERNAME));
        assertThrows(TokenNotFoundException.class, () -> authService.retrieveTokens(INVALID_USERNAME));
    }

    @Override
    protected void setField(String name, Object value, Object target) {
        super.setField(name, value, target);
    }
}
