insert into tbme_scope(name, description, created_at)
values
    ('RESOURCE_1_READ', 'Resource 1 Read', current_timestamp),
    ('RESOURCE_1_WRITE', 'Resource 1 Write', current_timestamp),
    ('RESOURCE_2_READ', 'Resource 2 Read', current_timestamp),
    ('RESOURCE_2_WRITE', 'Resource 2 Write', current_timestamp);
select @resource1ReadId  := id from tbme_scope where name = 'RESOURCE_1_READ';
select @resource1WriteId := id from tbme_scope where name = 'RESOURCE_1_WRITE';
select @resource2ReadId  := id from tbme_scope where name = 'RESOURCE_2_READ';
select @resource2WriteId := id from tbme_scope where name = 'RESOURCE_2_WRITE';

insert into tbme_resource(path, name, description, created_at)
values
    ('/resource-1', 'RESOURCE_1', 'Resource number 1', current_timestamp),
    ('/resource-2', 'RESOURCE_2', 'Resource number 1', current_timestamp);
select @resource1Id := id from tbme_resource where name = 'RESOURCE_1';
select @resource2Id := id from tbme_resource where name = 'RESOURCE_2';

insert into tbme_role(name, description, created_at)
values
    ('ADMIN', 'Admin Role', current_timestamp),
    ('READ_ONLY', 'Read Only Role', current_timestamp);
select @roleAdminId    := id from tbme_role where name = 'ADMIN';
select @roleReadOnlyId := id from tbme_role where name = 'READ_ONLY';

insert into tbme_user(username, password, unique_id, active, created_at)
values
    ('admin@email.com'       , '$2a$10$KH1hYEQ6hIruJ7dq4W2DuOYGqadr.CZUsdnteFxJPOYKhcBIDEO0y', 'c8afb570-dd5d-4c49-8bcd-0af9b47dc9d0', 1, current_timestamp),
    ('read_only@email.com'   , '$2a$10$KH1hYEQ6hIruJ7dq4W2DuOYGqadr.CZUsdnteFxJPOYKhcBIDEO0y', 'fc0d2e9e-f809-4ddd-8859-34fc8edcb984', 1, current_timestamp),
    ('no_role_user@email.com', '$2a$10$KH1hYEQ6hIruJ7dq4W2DuOYGqadr.CZUsdnteFxJPOYKhcBIDEO0y', '72dea1d7-3e83-4a87-a688-2e692750fce2', 1, current_timestamp);
select @userAdminId    := id from tbme_user where username = 'admin@email.com';
select @userReadOnlyId := id from tbme_user where username = 'read_only@email.com';

insert into tbme_user_role(user_id, role_id)
values
    (@userAdminId, @roleAdminId),
    (@userReadOnlyId, @roleReadOnlyId);

insert into tbme_role_resource_scope_mapping(role_id, resource_id, scope_id)
values
    (@roleAdminId, @resource1Id, @resource1ReadId),
    (@roleAdminId, @resource1Id, @resource1WriteId),
    (@roleAdminId, @resource2Id, @resource2ReadId),
    (@roleAdminId, @resource2Id, @resource2WriteId),
    (@roleReadOnlyId, @resource1Id, @resource1ReadId),
    (@roleReadOnlyId, @resource2Id, @resource2ReadId);
