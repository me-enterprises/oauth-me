package ca.me.oauthme.service.impl;

import ca.me.oauthme.core.util.JwtUtil;
import ca.me.oauthme.core.util.TokenUtil;
import ca.me.oauthme.model.db.User;
import ca.me.oauthme.repository.RoleResourceScopeMappingRepository;
import ca.me.oauthme.repository.UserRepository;
import ca.me.oauthme.service.AuthenticationService;
import ca.me.oauthme.service.dto.AuthenticationRequestDto;
import ca.me.oauthme.service.dto.AuthenticationResponseDto;
import ca.me.oauthme.service.exception.InvalidPasswordException;
import ca.me.oauthme.service.exception.TokenNotFoundException;
import ca.me.oauthme.service.exception.UnexpectedErrorException;
import ca.me.oauthme.service.exception.UserNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.MessageSource;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;

import static ca.me.oauthme.core.CacheConstants.CACHE_TOKEN;

/**
 * Class implementing all the business logic for authenticating an user in the system.
 *
 * @author Rodrigo da Silva
 * @version 1.0.0
 * @since 1.0.0
 */
@Slf4j
@Service
@CacheConfig(cacheNames = {CACHE_TOKEN})
public class AuthenticationServiceImpl implements AuthenticationService {

    private final PasswordEncoder passwordEncoder;
    private final UserRepository userRepository;
    private final RoleResourceScopeMappingRepository mappingRepository;
    private final MessageSource messages;
    private final JwtUtil jwtUtil;

    @Value("${oauth.token.refresh.size:128}")
    private int refreshTokenSize;

    @Value("${oauth.token.refresh.validity:60}")
    private long refreshTokenValidity;

    @Autowired
    public AuthenticationServiceImpl(PasswordEncoder passwordEncoder,
                                     UserRepository userRepository,
                                     RoleResourceScopeMappingRepository mappingRepository,
                                     MessageSource messages,
                                     JwtUtil jwtUtil) {
        this.passwordEncoder = passwordEncoder;
        this.userRepository = userRepository;
        this.mappingRepository = mappingRepository;
        this.jwtUtil = jwtUtil;
        this.messages = messages;
    }

    /**
     * @see AuthenticationService#login(AuthenticationRequestDto)
     */
    @Override
    @Transactional
    @CachePut(key = "#authRequest.username")
    public AuthenticationResponseDto login(AuthenticationRequestDto authRequest) throws UserNotFoundException, InvalidPasswordException, UnexpectedErrorException {
        try {
            final var username = authRequest.getUsername();
            final var password = authRequest.getPassword();
            log.info("##### :: AUTHENTICATION - Request received USERNAME: {}, PASSWORD: {}", username, password);

            // Validates user, updates last login date, builds the scopes, and generates JWT.
            var user = findUser(username);
            validatePassword(user, password);

            // Generates the JWT access token.
            final var accessToken = jwtUtil.generateToken(user.getUsername(), user.getUniqueId(), buildScopeList(user));
            log.info("##### :: AUTHENTICATION - Generated ACCESS_TOKEN: {}", accessToken);

            // Generates the refresh token.
            final var refreshToken = TokenUtil.generateSafeToken(refreshTokenSize);
            log.info("##### :: AUTHENTICATION - Generated REFRESH_TOKEN: {}", refreshToken);

            // Updates the refresh token and the last login date on the user and returns the data.
            updateLastLoginDateAndRefreshToken(user, refreshToken);
            return AuthenticationResponseDto
                .builder()
                .accessToken(accessToken)
                .refreshToken(refreshToken)
                .build();
        } catch (UserNotFoundException | InvalidPasswordException ex) {
            throw ex;
        } catch (Exception ex) {
            log.error("##### :: AUTHENTICATION - An unexpected error has happened in the application!", ex);
            throw new UnexpectedErrorException(messages, ex.getMessage());
        }
    }

    /**
     * @see AuthenticationService#logout(String)
     */
    @Override
    @CacheEvict(key = "#username")
    public boolean logout(String username) {
        log.info("##### :: AUTHENTICATION - LoggingOut USERNAME: {}", username);
        return true;
    }

    /**
     * @see AuthenticationService#retrieveTokens(String)
     */
    @Override
    @Cacheable(key = "#username")
    public AuthenticationResponseDto retrieveTokens(String username) throws TokenNotFoundException {
        throw new TokenNotFoundException(messages, username);
    }

    /**
     * @see AuthenticationService#getUsernameFromToken(String)
     */
    @Override
    public String getUsernameFromToken(String jwt) {
        if (StringUtils.isNotBlank(jwt)) {
            log.info("##### :: AUTHENTICATION - Getting the username from JWT: {}", jwt);
            String username = jwtUtil.getJwtUsername(jwt);
            log.info("##### :: AUTHENTICATION - Found USERNAME: {}", username);
            return username;
        }
        return StringUtils.EMPTY;
    }

    private User findUser(String username) throws UserNotFoundException {
        log.info("##### :: AUTHENTICATION - Searching for user with USERNAME: {}", username);
        final var opUser = userRepository.findByUsername(username);
        if (opUser.isEmpty()) {
            log.warn("##### :: AUTHENTICATION - User with USERNAME: {} not found!", username);
            throw new UserNotFoundException(messages, username);
        }
        final var user = opUser.get();
        log.info("##### :: AUTHENTICATION - Found USER: {}", user);
        return user;
    }

    private void validatePassword(User user, String password) throws InvalidPasswordException {
        if (!passwordEncoder.matches(password, user.getPassword())) {
            log.warn("##### :: AUTHENTICATION - Password doesn't match for USER: {}!", user.getUsername());
            throw new InvalidPasswordException(messages, user.getUsername());
        }
    }

    private void updateLastLoginDateAndRefreshToken(User user, String refreshToken) {
        log.info("##### :: AUTHENTICATION - Updating the last login date and REFRESH_TOKEN: {} for USER: {}", refreshToken, user);
        final var now = LocalDateTime.now(ZoneOffset.UTC);
        user.setLastLoginDate(now);
        user.setRefreshToken(refreshToken);
        user.setRefreshTokenValidity(now.plus(refreshTokenValidity, ChronoUnit.SECONDS));
        userRepository.save(user);
        log.debug("##### :: AUTHENTICATION - Updated USER: {}", user);
    }

    private ArrayList<String> buildScopeList(User user) {
        final var mappings = mappingRepository.findByUser(user);
        final var scopeList = new ArrayList<String>();
        mappings.forEach(m -> scopeList.add(m.toScope()));
        return scopeList;
    }
}
