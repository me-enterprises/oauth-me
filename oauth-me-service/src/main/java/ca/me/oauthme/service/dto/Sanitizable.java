package ca.me.oauthme.service.dto;

/**
 * Interface created to indicate what DTOs would call sanitization before proceeding with request.
 *
 * @param <T> The type that will be sanitized.
 * @author Rodrigo da Silva
 * @version 1.0.0
 * @since 1.0.0
 */
public interface Sanitizable<T> {

    /**
     * Method that will be called in order to sanitize the given type <b>T</b>.
     *
     * @return The sanitized object.
     */
    T sanitize();
}
