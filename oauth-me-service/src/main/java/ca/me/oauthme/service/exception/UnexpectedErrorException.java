package ca.me.oauthme.service.exception;

import org.springframework.context.MessageSource;

import static ca.me.oauthme.core.ErrorCodes.E_UNEXPECTED;

/**
 * Exception that will be thrown when an unexpected error occurs in the application.
 *
 * @author Rodrigo da Silva
 * @version 1.0.0
 * @since 1.0.0
 */
public class UnexpectedErrorException extends BusinessException {

    public UnexpectedErrorException(MessageSource message, Object... params) {
        super(E_UNEXPECTED, message, params);
    }
}
