package ca.me.oauthme.service;

import ca.me.oauthme.service.dto.AuthenticationRequestDto;
import ca.me.oauthme.service.dto.AuthenticationResponseDto;
import ca.me.oauthme.service.exception.InvalidPasswordException;
import ca.me.oauthme.service.exception.TokenNotFoundException;
import ca.me.oauthme.service.exception.UnexpectedErrorException;
import ca.me.oauthme.service.exception.UserNotFoundException;

/**
 * Interface containing all the necessary methods for dealing with authentication in the system.
 *
 * @author Rodrigo da Silva
 * @version 1.0.0
 * @since 1.0.0
 */
public interface AuthenticationService {
    /**
     * Authenticates the given username with the given password in the system.
     *
     * @param authRequest The authentication request containing the username and password.
     * @return The JWT token string.
     * @throws UserNotFoundException     when the {@link org.apache.catalina.User} with the given username doesn't exist in the system.
     * @throws InvalidPasswordException  when the {@link org.apache.catalina.User} is found but its password doesn't match the given password.
     * @throws UnexpectedErrorException  when any unexpected error occurs in the application.
     */
    AuthenticationResponseDto login(AuthenticationRequestDto authRequest) throws UserNotFoundException, InvalidPasswordException, UnexpectedErrorException;

    /**
     * Logs the user with the giver username out of the system.
     *
     * @param username The username to be logged out of the system.
     * @return <b>true</b> to indicate that the logout occurred successfully.
     */
    boolean logout(String username);

    /**
     * Retrieves the existing token from the cache.
     *
     * @param username The username is the key to be used to retrieve the token.
     * @return The tokens (access and refresh) found for the given username
     * @throws TokenNotFoundException if a token is not found for the given username.
     */
    AuthenticationResponseDto retrieveTokens(String username) throws TokenNotFoundException;

    /**
     * Gets the username from the token sent
     *
     * @param jwt the jwt to be parsed.
     * @return The username found on the token.
     */
    String getUsernameFromToken(String jwt);
}
