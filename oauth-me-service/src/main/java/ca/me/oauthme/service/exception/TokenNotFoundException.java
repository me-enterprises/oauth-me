package ca.me.oauthme.service.exception;

import org.springframework.context.MessageSource;

import static ca.me.oauthme.core.ErrorCodes.E_USER_TOKEN_NOT_FOUND;

/**
 * Exception that will be thrown when an a token is not found on the cache for a given username
 *
 * @author Rodrigo da Silva
 * @version 1.0.0
 * @since 1.0.0
 */
public class TokenNotFoundException extends BusinessException {

    public TokenNotFoundException(MessageSource message, Object... params) {
        super(E_USER_TOKEN_NOT_FOUND, message, params);
    }
}
