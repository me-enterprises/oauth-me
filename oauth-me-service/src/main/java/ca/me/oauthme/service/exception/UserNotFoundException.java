package ca.me.oauthme.service.exception;

import org.springframework.context.MessageSource;

import static ca.me.oauthme.core.ErrorCodes.E_USER_NOT_FOUND;

/**
 * Exception that will be thrown when an {@link org.apache.catalina.User} cannot be found on the system.
 *
 * @author Rodrigo da Silva
 * @version 1.0.0
 * @since 1.0.0
 */
public class UserNotFoundException extends BusinessException {

    public UserNotFoundException(MessageSource message, Object... params) {
        super(E_USER_NOT_FOUND, message, params);
    }
}
