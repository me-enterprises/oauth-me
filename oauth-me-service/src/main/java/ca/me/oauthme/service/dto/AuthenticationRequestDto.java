package ca.me.oauthme.service.dto;

import ca.me.oauthme.core.util.SanitizerUtil;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

/**
 * DTO class that will be used to transport authentication request data.
 *
 * @author Rodrigo da Silva
 * @version 1.0.0
 * @see Sanitizable
 * @since 1.0.0
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class AuthenticationRequestDto implements Sanitizable<AuthenticationRequestDto> {
    @NotBlank(message = "The username cannot be null or empty!")
    private String username;

    @NotBlank(message = "The password cannot be null or empty!")
    private String password;

    /**
     * @see Sanitizable#sanitize()
     */
    @Override
    public AuthenticationRequestDto sanitize() {
        username = SanitizerUtil.sanitize(username);
        password = SanitizerUtil.sanitize(password);
        return this;
    }
}
