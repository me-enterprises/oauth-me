package ca.me.oauthme.service.exception;

import ca.me.oauthme.core.ErrorCodes;
import org.springframework.context.MessageSource;

import java.util.Locale;

/**
 * Generic business exception class that MUST be extended by all the exceptions that will be properly thrown
 * on the business methods.
 *
 * @author Rodrigo da Silva
 * @version 1.0.0
 * @since 1.0.0
 */
public abstract class BusinessException extends Exception {

    private final ErrorCodes err;

    public BusinessException(ErrorCodes err) {
        super();
        this.err = err;
    }

    public BusinessException(ErrorCodes err, MessageSource message, Object... params) {
        super(message.getMessage(err.property(), params, Locale.getDefault()));
        this.err = err;
    }

    public BusinessException(ErrorCodes err, MessageSource message, Throwable t, Object... params) {
        super(message.getMessage(err.property(), params, Locale.getDefault()), t);
        this.err = err;
    }

    public int getCode() {
        return err.code();
    }
}
