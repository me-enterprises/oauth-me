package ca.me.oauthme.service.exception;

import org.springframework.context.MessageSource;

import static ca.me.oauthme.core.ErrorCodes.E_USER_INVALID_PASSWORD;

/**
 * Exception that will be thrown when an {@link org.apache.catalina.User}'s password do not match the
 * password that was sent.
 *
 * @author Rodrigo da Silva
 * @version 1.0.0
 * @since 1.0.0
 */
public class InvalidPasswordException extends BusinessException {

    public InvalidPasswordException(MessageSource message, Object... params) {
        super(E_USER_INVALID_PASSWORD, message, params);
    }
}
