CREATE TABLE tbme_scope
(
    id              INT          NOT NULL PRIMARY KEY AUTO_INCREMENT,
    name            VARCHAR(30)  NOT NULL,
    description     VARCHAR(150) NOT NULL,
    created_at      TIMESTAMP    NOT NULL,
    last_updated_at TIMESTAMP    NULL,
    UNIQUE KEY ux_scope__name (name)
) ENGINE = INNODB,
  DEFAULT CHARSET = UTF8;

CREATE TABLE tbme_resource
(
    id              INT          NOT NULL PRIMARY KEY AUTO_INCREMENT,
    path            VARCHAR(256) NOT NULL,
    name            VARCHAR(30)  NOT NULL,
    description     VARCHAR(150) NOT NULL,
    created_at      TIMESTAMP    NOT NULL,
    last_updated_at TIMESTAMP    NULL,
    UNIQUE KEY ux_resource__name (name),
    UNIQUE KEY ux_resource__path (path)
) ENGINE = INNODB,
  DEFAULT CHARSET = UTF8;

CREATE TABLE tbme_role
(
    id              INT          NOT NULL PRIMARY KEY AUTO_INCREMENT,
    name            VARCHAR(30)  NOT NULL,
    description     VARCHAR(150) NOT NULL,
    created_at      TIMESTAMP    NOT NULL,
    last_updated_at TIMESTAMP    NULL,
    UNIQUE KEY ux_role__name (name)
) ENGINE = INNODB,
  DEFAULT CHARSET = UTF8;


CREATE TABLE tbme_user
(
    id                      BIGINT(20)   NOT NULL PRIMARY KEY AUTO_INCREMENT,
    username                VARCHAR(50)  NOT NULL,
    password                VARCHAR(60)  NOT NULL,
    unique_id               VARCHAR(36)  NOT NULL,
    active                  BIT          NOT NULL,
    last_login_date         TIMESTAMP    NULL,
    created_at              TIMESTAMP    NOT NULL,
    last_updated_at         TIMESTAMP    NULL,
    refresh_token           VARCHAR(256) NULL,
    refresh_token_validity  TIMESTAMP NULL,

    UNIQUE KEY ux_user__username (username),
    UNIQUE KEY ux_user__unique_id (unique_id),
    UNIQUE KEY ux_user__refresh_token (refresh_token)
) ENGINE = INNODB,
  DEFAULT CHARSET = UTF8;

CREATE TABLE tbme_user_role
(
    user_id BIGINT(20) NOT NULL,
    role_id INT        NOT NULL,
    PRIMARY KEY pk_user_role__user_id__role_id (user_id, role_id),

    INDEX ix_user_role__user_id (user_id),
    INDEX ix_user_role__role_id (role_id),

    FOREIGN KEY fk_user_role__user_id (user_id)
        REFERENCES tbme_user (id),
    FOREIGN KEY fk_user_role__role_id (role_id)
        REFERENCES tbme_role (id)
) ENGINE = INNODB,
  DEFAULT CHARSET = UTF8;

CREATE TABLE tbme_role_resource_scope_mapping
(
    role_id     INT NOT NULL,
    resource_id INT NOT NULL,
    scope_id    INT NOT NULL,
    PRIMARY KEY pk_role_resource_scope_mapping__role_id__resource_id__scope_id (role_id, resource_id, scope_id),

    INDEX ix_role_resource_scope_mapping__role_id (role_id),
    INDEX ix_role_resource_scope_mapping__resource_id (resource_id),
    INDEX ix_role_resource_scope_mapping__scope_id (scope_id),

    FOREIGN KEY fk_role_resource_scope_mapping__role_id (role_id)
        REFERENCES tbme_role (id),
    FOREIGN KEY fk_role_resource_scope_mapping__resource_id (resource_id)
        REFERENCES tbme_resource (id),
    FOREIGN KEY fk_role_resource_scope_mapping__scope_id (scope_id)
        REFERENCES tbme_scope (id)
) ENGINE = INNODB,
  DEFAULT CHARSET = UTF8;