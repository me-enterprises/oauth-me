package ca.me.oauthme.model.db;

import ca.me.oauthme.model.db.pk.UserRolePk;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * Class representing the mapping among an {@link User} to one or more {@link Role}s.
 *
 * @author Rodrigo da Silva
 * @version 1.0.0
 * @since 1.0.0
 * @see User
 * @see Role
 */
@Getter
@Setter
@ToString
@EqualsAndHashCode
@Entity
@Table(name = "tbme_user_role")
public class UserRole implements Serializable {
    @EmbeddedId
    private UserRolePk id;
}
