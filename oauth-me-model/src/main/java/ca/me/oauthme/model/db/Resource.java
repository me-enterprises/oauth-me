package ca.me.oauthme.model.db;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * Class representing a given resource that will be assigned to the a given user for a given scope.
 *
 * @author Rodrigo da Silva
 * @version 1.0.0
 * @since 1.0.0
 */
@Getter
@Setter
@ToString
@EqualsAndHashCode(of = {"path"}, callSuper = false)
@Entity
@Table(name = "tbme_resource", uniqueConstraints = {
        @UniqueConstraint(name = "ux_resource__name", columnNames = {"name"}),
        @UniqueConstraint(name = "ux_resource__path", columnNames = {"path"})
})
public class Resource extends EntityBase<Integer> {
    @Column(name = "path", unique = true, nullable = false, length = 256)
    private String path;

    @Column(name = "name", unique = true, nullable = false, length = 30)
    private String name;

    @Column(name = "description", nullable = false, length = 150)
    private String description;
}
