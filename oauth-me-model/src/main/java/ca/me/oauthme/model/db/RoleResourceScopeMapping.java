package ca.me.oauthme.model.db;

import ca.me.oauthme.model.db.pk.RoleResourceScopeMappingPk;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * Class representing the mapping among an {@link Role} with its authorized {@link Scope}s on {@link Resource}s.
 *
 * @author Rodrigo da Silva
 * @version 1.0.0
 * @see Role
 * @see Scope
 * @see Resource
 * @since 1.0.0
 */
@Getter
@Setter
@ToString
@EqualsAndHashCode
@Entity
@Table(name = "tbme_role_resource_scope_mapping")
public class RoleResourceScopeMapping implements Serializable {
    @EmbeddedId
    private RoleResourceScopeMappingPk id;

    public String toScope() {
        return String.format("%s::%s", id.getResource().getName(), id.getScope().getName());
    }
}
