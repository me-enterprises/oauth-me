package ca.me.oauthme.model.db;

import ca.me.oauthme.model.db.audit.Audit;
import ca.me.oauthme.model.db.audit.AuditListener;
import ca.me.oauthme.model.db.audit.Auditable;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/**
 * Base entity that will be extended by any Entity in the application model structure.
 *
 * @author Rodrigo da Silva
 * @version 1.0.0
 * @since 1.0.0
 */
@Getter
@Setter
@ToString
@EqualsAndHashCode(of = {"id"}, callSuper = false)
@EntityListeners(AuditListener.class)
@MappedSuperclass
public abstract class EntityBase<T extends Number> implements Auditable {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private T id;

    @Embedded
    private Audit audit;
}
