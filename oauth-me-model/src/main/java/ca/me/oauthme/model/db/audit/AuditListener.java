package ca.me.oauthme.model.db.audit;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * Audit listener to be used by Hibernate entities in order to set auditable fields.
 *
 * @author Rodrigo da Silva
 * @version 1.0.0
 * @since 1.0.0
 */
public class AuditListener {
    /**
     * Sets the created at value before persisting the data.
     *
     * @param o The auditable object to be used.
     */
    @PrePersist
    public void setCreatedAt(Object o) {
        if (o instanceof Auditable) {
            Auditable auditable = (Auditable) o;
            Audit audit = auditable.getAudit();

            if (Objects.isNull(audit)) {
                audit = new Audit();
                auditable.setAudit(audit);
            }

            audit.setCreatedAt(ZonedDateTime.now(ZoneOffset.UTC));
        }
    }

    /**
     * Sets the last updated at value before updating the data.
     *
     * @param o The auditable object to be used.
     */
    @PreUpdate
    public void setLastUpdatedAt(Object o) {
        if (o instanceof Auditable) {
            Auditable auditable = (Auditable) o;
            Audit audit = auditable.getAudit();

            if (Objects.isNull(audit)) {
                audit = new Audit();
                auditable.setAudit(audit);
            }

            audit.setLastUpdatedAt(ZonedDateTime.now(ZoneOffset.UTC));
        }
    }
}
