package ca.me.oauthme.model.db;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * Class representing a given scope that will be assigned to the a given user for a given resource.
 *
 * @author Rodrigo da Silva
 * @version 1.0.0
 * @since 1.0.0
 */
@Getter
@Setter
@ToString
@EqualsAndHashCode(of = {"name"}, callSuper = false)
@Entity
@Table(name = "tbme_scope", uniqueConstraints = {
        @UniqueConstraint(name = "ux_scope__name", columnNames = {"name"})
})
public class Scope extends EntityBase<Integer> {
    @Column(name = "name", unique = true, nullable = false, length = 30)
    private String name;

    @Column(name = "description", nullable = false, length = 150)
    private String description;
}
