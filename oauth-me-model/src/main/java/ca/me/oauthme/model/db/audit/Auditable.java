package ca.me.oauthme.model.db.audit;

import java.io.Serializable;

/**
 * Auditable interface to be used with auditing purposes.
 *
 * @author Rodrigo da Silva
 * @version 1.0.0
 * @since 1.0.0
 */
public interface Auditable extends Serializable {
    /**
     * Retrieves the audit to be used.
     *
     * @return The audit to be used.
     */
    Audit getAudit();

    /**
     * Sets the audit to be used.
     *
     * @param audit the audit to be set.
     */
    void setAudit(Audit audit);
}
