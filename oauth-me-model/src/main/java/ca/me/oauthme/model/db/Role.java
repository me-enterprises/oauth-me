package ca.me.oauthme.model.db;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import java.util.HashSet;
import java.util.Set;

/**
 * Class representing a given role that will be assigned to the a given user.
 *
 * @author Rodrigo da Silva
 * @version 1.0.0
 * @since 1.0.0
 */
@Getter
@Setter
@ToString
@EqualsAndHashCode(of = {"name"}, callSuper = false)
@Entity
@Table(name = "tbme_role", uniqueConstraints = {
        @UniqueConstraint(name = "ux_role__name", columnNames = {"name"})
})
public class Role extends EntityBase<Integer> {
    @Column(name = "name", unique = true, nullable = false, length = 30)
    private String name;

    @Column(name = "description", nullable = false, length = 150)
    private String description;

    @OneToMany(mappedBy = "id.role")
    private Set<UserRole> userRoles = new HashSet<>();
}
