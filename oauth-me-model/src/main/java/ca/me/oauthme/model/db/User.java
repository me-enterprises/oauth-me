package ca.me.oauthme.model.db;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import java.time.LocalDateTime;

/**
 * Class representing a given resource that will be assigned to the a given user for a given scope.
 *
 * @author Rodrigo da Silva
 * @version 1.0.0
 * @since 1.0.0
 */
@Getter
@Setter
@ToString
@EqualsAndHashCode(of = {"username"}, callSuper = false)
@Entity
@Table(name = "tbme_user", uniqueConstraints = {
    @UniqueConstraint(name = "ux_user__username", columnNames = {"username"}),
    @UniqueConstraint(name = "ux_user__unique_id", columnNames = {"unique_id"}),
    @UniqueConstraint(name = "ux_user__refresh_token", columnNames = {"refresh_token"})
})
public class User extends EntityBase<Long> {
    @Column(name = "username", unique = true, nullable = false, length = 50)
    private String username;

    @Column(name = "password", nullable = false, length = 60)
    private String password;

    @Column(name = "unique_id", nullable = false, length = 36)
    private String uniqueId;

    @Column(name = "active", nullable = false)
    private boolean active = false;

    @Column(name = "last_login_date")
    private LocalDateTime lastLoginDate;

    @Column(name = "refresh_token", unique = true, length = 256)
    private String refreshToken;

    @Column(name = "refresh_token_validity")
    private LocalDateTime refreshTokenValidity;
}
