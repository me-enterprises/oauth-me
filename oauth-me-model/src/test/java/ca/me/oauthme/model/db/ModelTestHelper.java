package ca.me.oauthme.model.db;

import ca.me.oauthme.model.db.pk.RoleResourceScopeMappingPk;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * Helper class to be used in tests that needs an {@link User} to be built.
 *
 * @author Rodrigo da Silva
 * @version 1.0.0
 * @since 1.0.0
 */
public class ModelTestHelper {
    private static final PasswordEncoder PASSWORD_ENCODER = new BCryptPasswordEncoder();

    /**
     * Creates a new {@link User} with the given username, password, and uniqueId.
     *
     * @param username The username to be set.
     * @param password The password to be set.
     * @param uniqueId The uniqueId to be set.
     * @return The created {@link User}
     */
    public static User newUser(String username, String password, String uniqueId) {
        final var user = new User();
        user.setUsername(username);
        user.setPassword(PASSWORD_ENCODER.encode(password));
        user.setUniqueId(uniqueId);
        return user;
    }

    /**
     * Creates a new {@link Role} with the given name.
     *
     * @param name The name to be set.
     * @return The created {@link User}
     */
    public static Role newRole(String name) {
        final var role = new Role();
        role.setName(name);
        return role;
    }

    /**
     * Creates a new {@link Resource} with the given name and path.
     *
     * @param name The name to be set.
     * @param path The path to be set
     * @return The created {@link Resource}
     */
    public static Resource newResource(String name, String path) {
        final var resource = new Resource();
        resource.setName(name);
        resource.setPath(path);
        return resource;
    }

    /**
     * Creates a new {@link Scope} with the given name.
     *
     * @param name The name to be set.
     * @return The created {@link Scope}
     */
    public static Scope newScope(String name) {
        final var scope = new Scope();
        scope.setName(name);
        return scope;
    }

    /**
     * Creates a new {@link RoleResourceScopeMapping} with the given role, resource, and scope.
     *
     * @param role     The role to be set.
     * @param resource The resource to be set.
     * @param scope    The scope to be set.
     * @return The created {@link RoleResourceScopeMapping}
     */
    public static RoleResourceScopeMapping newMapping(Role role, Resource resource, Scope scope) {
        final var mapping = new RoleResourceScopeMapping();
        mapping.setId(new RoleResourceScopeMappingPk());
        mapping.getId().setRole(role);
        mapping.getId().setResource(resource);
        mapping.getId().setScope(scope);
        return mapping;
    }
}
