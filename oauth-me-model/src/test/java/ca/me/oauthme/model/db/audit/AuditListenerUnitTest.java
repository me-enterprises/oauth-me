package ca.me.oauthme.model.db.audit;

import ca.me.oauthme.model.db.User;
import org.junit.jupiter.api.Test;
import org.springframework.data.util.ReflectionUtils;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * Test class created to Unit Test the class {@link AuditListener}
 *
 * @author Rodrigo da Silva
 * @version 1.0.0
 * @since 1.0.0
 * @see AuditListener
 */
class AuditListenerUnitTest {
    private static final String GET_AUDIT = "getAudit";

    private final AuditListener auditListener = new AuditListener();

    @Test
    void testSetCreatedAtForAuditableWhenAuditIsNull() {
        User user = new User();
        auditListener.setCreatedAt(user);
        assertNotNull(user.getAudit().getCreatedAt());
    }

    @Test
    void testSetCreatedAtForAuditableWhenAuditIsNotNull() {
        final User user = new User();
        user.setAudit(new Audit());
        auditListener.setCreatedAt(user);
        assertNotNull(user.getAudit().getCreatedAt());
    }

    @Test
    void testSetCreatedAtForNonAuditable() {
        final Object o = new Object();
        auditListener.setCreatedAt(o);
        assertFalse(ReflectionUtils.getMethod(Object.class,GET_AUDIT).isPresent());
    }

    @Test
    void testSetLastUpdatedAtWhenAuditIsNull() {
        final User user = new User();
        auditListener.setLastUpdatedAt(user);
        assertNotNull(user.getAudit().getLastUpdatedAt());
    }

    @Test
    void testSetLastUpdatedAtWhenAuditIsNotNull() {
        final User user = new User();
        user.setAudit(new Audit());
        auditListener.setLastUpdatedAt(user);
        assertNotNull(user.getAudit().getLastUpdatedAt());
    }

    @Test
    void testSetLastUpdatedAtForNonAuditable() {
        final Object o = new Object();
        auditListener.setLastUpdatedAt(o);
        assertFalse(ReflectionUtils.getMethod(Object.class,GET_AUDIT).isPresent());
    }
}
