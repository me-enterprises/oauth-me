package ca.me.oauthme.core.util;

import org.apache.commons.lang3.StringUtils;

import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;
import java.util.Base64;
import java.util.Date;

/**
 * Utility class to deal with non JWT tokens.
 *
 * @author Rodrigo da Silva
 * @version 1.0.0
 * @since 1.0.0
 */
public abstract class TokenUtil {

    public static final String AUTH_HEADER_PREFIX = "Bearer ";

    private TokenUtil() {
    }

    /**
     * Method created to properly extract the token sent on the authorization header.
     *
     * @param authHeader The authorization header received.
     * @return The token if the header starts with the proper prefix. An empty string otherwise.
     */
    public static String extractJwtFromAuthHeader(String authHeader) {
        if (StringUtils.isNotBlank(authHeader) && authHeader.startsWith(AUTH_HEADER_PREFIX)) {
            return authHeader.substring(AUTH_HEADER_PREFIX.length());
        }

        return StringUtils.EMPTY;
    }

    public static String generateSafeToken(int size) {
        SecureRandom random = new SecureRandom(new Date().toString().getBytes(StandardCharsets.UTF_8));
        byte[] bytes = new byte[size];
        random.nextBytes(bytes);
        Base64.Encoder encoder = Base64.getUrlEncoder().withoutPadding();
        return encoder.encodeToString(bytes).substring(0, size);
    }
}
