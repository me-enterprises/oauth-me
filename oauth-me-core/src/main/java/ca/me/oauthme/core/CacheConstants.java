package ca.me.oauthme.core;

/**
 * Abstract class containing constants for cache names.
 *
 * @author Rodrigo da Silva
 * @version 1.0.0
 * @since 1.0.0
 */
public abstract class CacheConstants {

    public static final String CACHE_TOKEN = "token-cache";

    private CacheConstants() {
    }
}
