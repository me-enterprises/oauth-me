package ca.me.oauthme.core.util;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Helper class that will be use in user parameter sanitization to avoid tainted data.
 *
 * @author Rodrigo da Silva
 * @version 1.0.0
 * @since 1.0.0
 */
public final class SanitizerUtil {

    private static final String REGEX = "(?i)(select)|(from)|(inner)|(outer)|(join)|(where)|(order by)|(group by)|(having)|(<[^>]*>)|[\r\n\t]";

    /**
     * Hides the constructor from class users.
     */
    private SanitizerUtil() {
    }

    public static Date sanitize(Date tobeSanitized) {
        if (Objects.nonNull(tobeSanitized)) {
            Calendar cal = Calendar.getInstance();
            cal.setLenient(false);
            cal.setTime(tobeSanitized);
            return cal.getTime();
        }
        return null;
    }

    public static List<String> sanitize(List<String> toBeSanitized) {
        if (CollectionUtils.isNotEmpty(toBeSanitized)) {
            return toBeSanitized
                    .stream()
                    .map(SanitizerUtil::sanitize)
                    .collect(Collectors.toList());
        }
        return Collections.emptyList();
    }

    public static String[] sanitize(String[] toBeSanitized) {
        if (Objects.nonNull(toBeSanitized) && toBeSanitized.length > 0) {
            return Arrays.stream(toBeSanitized)
                    .map(SanitizerUtil::sanitize)
                    .toArray(String[]::new);
        }
        return new String[0];
    }

    public static String sanitize(String toBeSanitized) {
        return sanitize(toBeSanitized, StringUtils.EMPTY);
    }

    public static String sanitize(String toBeSanitized, String replacement) {
        if (Objects.isNull(toBeSanitized)) {
            return null;
        }

        if (StringUtils.isBlank(toBeSanitized)) {
            return StringUtils.EMPTY;
        }

        replacement = Objects.isNull(replacement) ? StringUtils.EMPTY : replacement;
        return toBeSanitized.replaceAll(REGEX, replacement);
    }
}
