package ca.me.oauthme.core.util;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.impl.DefaultClaims;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.function.Function;

/**
 * Utility class that will help with JWT creation, validation, and other JWT common operations.
 *
 * @author Rodrigo da Silva
 * @version 1.0.0
 * @since 1.0.0
 */
@Slf4j
@Component
public class JwtUtil {

    private static final String ISSUER = "Me Enterprises";
    private static final String SCOPE = "scope";

    @Value("${OAUTHME_JWT_SECRET_KEY}")
    private String jwtSecretKey;

    @Value("${OAUTHME_JWT_VALIDITY:3600000}")
        private int jwtValidity;

    /**
     * Helper method to generate a JWT token containing the given subject, audience, and custom claims.
     *
     * @param subject  The subject to be used.
     * @param audience The audience to be used.
     * @param scopes   The scopes to be added.
     * @return The generated JWT token.
     */
    public String generateToken(String subject, String audience, List<String> scopes) {
        final var genDate = new Date();
        log.debug("##### :: JWT - GenerationDate: {}", genDate);
        final var expDate = new Date(genDate.getTime() + jwtValidity);
        log.debug("##### :: JWT - ExpirationDate: {}", expDate);
        final var jwtBuilder = Jwts.builder()
                .setId(UUID.randomUUID().toString())
                .setIssuer(ISSUER)
                .setIssuedAt(genDate)
                .setNotBefore(genDate)
                .setExpiration(expDate)
                .setSubject(subject)
                .setAudience(audience)
                .signWith(SignatureAlgorithm.HS512, jwtSecretKey);

        final var scopesMap = new HashMap<String, Object>();
        if (CollectionUtils.isNotEmpty(scopes)) {
            log.debug("##### :: JWT - Scopes: {}", scopes);
            scopesMap.putIfAbsent(SCOPE, scopes);
        } else {
            scopesMap.putIfAbsent(SCOPE, new ArrayList<String>());
        }
        jwtBuilder.addClaims(scopesMap);

        final var jwt = jwtBuilder.compact();
        log.info("##### :: JWT - JWT: {}, with SUB: {}, AUD: {}, GEN: {}, EXP: {}", jwt, subject, audience, genDate.getTime(), expDate.getTime());
        return jwt;
    }

    /**
     * Validates the given JWT validity. It checks if the subject, audience, and issuer matches the correct values
     * and if the JWT is not expired, meaning the current date is after the notBefore and before the expiration claims.
     *
     * @param jwt      The JWT to be validated
     * @param subject  The subject to validate the JWT against.
     * @param audience The audience to validate the JWT against.
     * @return <b>true</b> if the JWT is valid. <b>false</b> otherwise.
     */
    public boolean isJwtValid(String jwt, String subject, String audience) {
        try {
            final var claims = getAllJwtClaims(jwt);
            return subject.equals(claims.getSubject()) &&
                    audience.equals(claims.getAudience());
        } catch (ExpiredJwtException ex) {
            log.warn("##### :: JWT - JWT expired!", ex);
        }

        return false;
    }

    /**
     * Retrieves the subject (username) from the given JWT Token
     *
     * @param jwt the JWT to be parsed in order to retrieve the username.
     * @return The found username.
     */
    public String getJwtUsername(String jwt) {
        return getJwtClaim(jwt, Claims::getSubject);
    }

    /**
     * Retrieves the scope (rights) from the given JWT Token.
     *
     * @param jwt the JWT to be parsed in order to retrieve the scope.
     * @return A set containing the scopes found.
     */
    @SuppressWarnings("unchecked")
    public List<String> getJwtScope(String jwt) {
        final var claims = getAllJwtClaims(jwt);
        return claims.get(SCOPE, List.class);
    }

    private <T> T getJwtClaim(String jwt, Function<Claims, T> function) {
        final var claim = function.apply(getAllJwtClaims(jwt));
        log.debug("##### :: JWT Claim: {} is being returned.", claim);
        return claim;
    }

    Claims getAllJwtClaims(String jwt) {
        try {
            return Jwts.parser()
                .setSigningKey(jwtSecretKey)
                .parseClaimsJws(jwt)
                .getBody();
        } catch(MalformedJwtException ex) {
            log.warn("##### :: JWT - An error has occurred when retrieving all claims.", ex);
            return new DefaultClaims();
        }
    }
}
