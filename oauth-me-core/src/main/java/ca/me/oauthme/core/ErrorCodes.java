package ca.me.oauthme.core;

/**
 * Enumeration containing all the possible error codes on the application.
 *
 * @author Rodrigo da Silva
 * @version 1.0.0
 * @since 1.0.0
 */
public enum ErrorCodes {
    E_OK(0, "error.ok"),

    E_PARAM_INVALID(10000, "error.param.invalid"),
    E_USER_NOT_FOUND(10001, "error.user.not-found"),
    E_USER_INVALID_PASSWORD(10002, "error.user.invalid-password"),
    E_USER_TOKEN_NOT_FOUND(10003, "error.user.token-not-found"),

    E_CREDENTIALS_INVALID(20000, "error.credentials.invalid"),

    E_JWT_EXPIRED(40000, "error.jwt.expired"),

    E_UNEXPECTED(90000, "error.unexpected");

    private int code;
    private String property;

    ErrorCodes(int code, String property) {
        this.code = code;
        this.property = property;
    }

    public int code() {
        return code;
    }

    public String property() {
        return property;
    }
}
