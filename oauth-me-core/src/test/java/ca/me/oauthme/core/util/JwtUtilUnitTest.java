package ca.me.oauthme.core.util;

import ca.me.oauthme.AbstractUnitTest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.Callable;

import static org.awaitility.Awaitility.await;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Unit Test class created for testing the {@link JwtUtil} class.
 *
 * @author Rodrigo da Silva
 * @version 1.0.0
 * @since 1.0.0
 */
class JwtUtilUnitTest extends AbstractUnitTest {

    private static final String SUB = "subject";
    private static final String AUD = "audience";
    private static final int jwtValidity = 2500;

    private final JwtUtil jwtUtil = new JwtUtil();

    private List<String> scope;

    @BeforeEach
    void setUp() {
        // Injects jwtSecretKey and jwtValidity in seconds manually.
        setField("jwtSecretKey", "jwtSecretKeyForTesting!@#$%^&*()_+", jwtUtil);
        setField("jwtValidity", jwtValidity, jwtUtil);
        if (Objects.isNull(scope)) {
            buildScope();
        }
    }

    @Test
    void testGetJwtUsername_whenSuccess() {
        final var jwt = jwtUtil.generateToken(SUB, AUD, scope);
        assertEquals(SUB, jwtUtil.getJwtUsername(jwt));
    }

    @Test
    void testGetJwtUsername_whenMalformedJwt() {
        assertNull(jwtUtil.getJwtUsername("INVALID_JWT"));
    }

    @Test
    void testGetJwtScope_whenSuccess_withNoScope() {
        final var jwt = jwtUtil.generateToken(SUB, AUD, null);
        assertTrue(jwtUtil.getJwtScope(jwt).isEmpty());
    }

    @Test
    void testGetJwtScope_whenSuccess_withScope() {
        final var jwt = jwtUtil.generateToken(SUB, AUD, scope);
        final var scope = jwtUtil.getJwtScope(jwt);
        assertEquals(1, scope.size());
        assertTrue(scope.contains("Authenticate::OAUTH_AUTHENTICATE_READ"));
    }

    @Test
    void testGenerateToken_whenSuccess_withCustomClaims() {
        final var jwt = jwtUtil.generateToken(SUB, AUD, scope);
        assertNotNull(jwt);
        final var claims = jwtUtil.getAllJwtClaims(jwt);
        assertEquals(8, claims.size());
    }

    @Test
    void    testGenerateToken_whenSuccess_withNoCustomClaims() {
        final var jwt = jwtUtil.generateToken(SUB, AUD, null);
        assertNotNull(jwt);
        final var claims = jwtUtil.getAllJwtClaims(jwt);
        assertEquals(8, claims.size());
        assertTrue(jwtUtil.getJwtScope(jwt).isEmpty());
    }

    @Test
    void testValidateToken_whenSuccess() {
        final var jwt = jwtUtil.generateToken(SUB, AUD, scope);
        assertTrue(jwtUtil.isJwtValid(jwt, SUB, AUD));
    }

    @Test
    void testValidateToken_whenTokenExpired() {
        final var jwt = jwtUtil.generateToken(SUB, AUD, scope);
        await().forever().until(tokenExpired(jwt), equalTo(true));
        assertFalse(jwtUtil.isJwtValid(jwt, SUB, AUD));
    }

    @Test
    void testValidateToken_whenInvalidSubject() {
        final var jwt = jwtUtil.generateToken(SUB, AUD, scope);
        assertFalse(jwtUtil.isJwtValid(jwt, "SUBJECT_INVALID", AUD));
    }

    @Test
    void testValidateToken_whenInvalidAudience() {
        final var jwt = jwtUtil.generateToken(SUB, AUD, scope);
        assertFalse(jwtUtil.isJwtValid(jwt, SUB, "AUDIENCE_INVALID"));
    }

    private void buildScope() {
        scope = new ArrayList<>();
        scope.add("Authenticate::OAUTH_AUTHENTICATE_READ");
    }

    private Callable<Boolean> tokenExpired(String jwt) {
        return () -> !jwtUtil.isJwtValid(jwt, JwtUtilUnitTest.SUB, JwtUtilUnitTest.AUD);
    }
}
