package ca.me.oauthme.core.util;

import ca.me.oauthme.AbstractUnitTest;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Tester class for the {@link SanitizerUtil} class.
 *
 * @author Rodrigo da Silva
 * @version 1.0.0
 * @since 1.0.0
 */
class SanitizerUtilTest extends AbstractUnitTest {

    @Test
    void testSanitizer_withSQL() {
        String toBeSanitized = "SELEct * FrOm TABLE1 inneR Join TABLE2\twhere X = Y order BY X gRoup By Y\r\n";
        String expected = " *  TABLE1   TABLE2 X = Y  X  Y";
        assertEquals(expected, SanitizerUtil.sanitize(toBeSanitized));
    }

    @Test
    void testSanitizer_withHTML() {
        String toBeSanitized = "<html><body><h1>Testing</H1></bOdy></HTML>";
        String expected = "Testing";
        assertEquals(expected, SanitizerUtil.sanitize(toBeSanitized));
    }

    @Test
    void testSanitizer_withNullDate() {
        assertNull(SanitizerUtil.sanitize((Date) null));
    }

    @Test
    void testSanitizer_withValidDate() {
        assertNotNull(SanitizerUtil.sanitize(new Date()));
    }

    @Test
    void testSanitizer_withNullString() {
        assertNull(SanitizerUtil.sanitize((String)null));
    }

    @Test
    void testSanitizer_withNullList() {
        assertTrue(SanitizerUtil.sanitize((List<String>)null).isEmpty());
    }

    @Test
    void testSanitizer_withEmptyList() {
        assertTrue(SanitizerUtil.sanitize(Collections.emptyList()).isEmpty());
    }

    @Test
    void testSanitizer_withListSafeStrings() {
        assertNotNull(SanitizerUtil.sanitize(Arrays.asList("-", "help", "-", "h")));
        assertEquals(4, SanitizerUtil.sanitize(Arrays.asList("-", "help", "-", "h")).size());
    }

    @Test
    void testSanitizer_withListNonSafeStrings() {
        List<String> sanitized = SanitizerUtil.sanitize(Arrays.asList("select", "from"));
        assertNotNull(SanitizerUtil.sanitize(sanitized));
        assertEquals(2, sanitized.size());
        assertEquals("", sanitized.get(0) );
        assertEquals("", sanitized.get(1) );
    }

    @Test
    void testSanitizer_withNullArray() {
        assertNotNull(SanitizerUtil.sanitize((String[])null));
        assertEquals(0, SanitizerUtil.sanitize((String[]) null).length);
    }

    @Test
    void testSanitizer_withEmptyArray() {
        assertNotNull(SanitizerUtil.sanitize(new String[0]));
        assertEquals(0, SanitizerUtil.sanitize(new String[0]).length);
    }

    @Test
    void testSanitizer_withArraySafeStrings() {
        assertNotNull(SanitizerUtil.sanitize(new String[]{"-", "help", "-", "h"}));
        assertEquals(4, SanitizerUtil.sanitize(new String[]{"-", "help", "-", "h"}).length);
    }

    @Test
    void testSanitizer_withArrayNonSafeStrings() {
        String[] sanitized = SanitizerUtil.sanitize(new String[]{"select", "from"});
        assertNotNull(SanitizerUtil.sanitize(sanitized));
        assertEquals(2, sanitized.length);
        assertEquals("", sanitized[0] );
        assertEquals("", sanitized[1] );
    }

    @Test
    void testSanitizer_withNullReplacement() {
        String toBeSanitized = "<html><body><h1>Testing</H1></bOdy></HTML>";
        String expected = "Testing";
        assertEquals(expected, SanitizerUtil.sanitize(toBeSanitized, null));
    }

    @Test
    void testSanitizer_withCustomReplacement() {
        String toBeSanitized = "<html><body><h1>Testing</H1></bOdy></HTML>";
        String expected = "###Testing###";
        assertEquals(expected, SanitizerUtil.sanitize(toBeSanitized, "#"));
    }

    @Test
    void testSanitizer_withNoHTMLAndNoSQL() {
        String toBeSanitized = "Testing";
        assertEquals(toBeSanitized, SanitizerUtil.sanitize(toBeSanitized, "#"));
    }

    @Test
    void testSanitizer_withEmail() {
        String toBeSanitized = "test@email.com";
        assertEquals(toBeSanitized, SanitizerUtil.sanitize(toBeSanitized));
    }
}

