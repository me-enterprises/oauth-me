package ca.me.oauthme.core.util;

import ca.me.oauthme.AbstractUnitTest;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Unit Test class for {@link TokenUtil}
 *
 * @author Rodrigo da Silva
 * @version 1.0.0
 * @since 1.0.0
 */
class TokenUtilUnitTest extends AbstractUnitTest {

    private static final String TOKEN = "THIS_IS_THE_TOKEN";

    @Test
    void testExtractJwtFromAuthHeader_returningToken() {
        final var authHeader = TokenUtil.AUTH_HEADER_PREFIX + TOKEN;
        assertEquals(TOKEN, TokenUtil.extractJwtFromAuthHeader(authHeader));
    }

    @Test
    void testExtractJwtFromAuthHeader_withNullAuthHeader_returningToken() {
        assertEquals(StringUtils.EMPTY, TokenUtil.extractJwtFromAuthHeader(null));
    }

    @Test
    void testExtractJwtFromAuthHeader_withEmptyAuthHeader_returningToken() {
        assertEquals(StringUtils.EMPTY, TokenUtil.extractJwtFromAuthHeader(StringUtils.EMPTY));
    }

    @Test
    void testExtractJwtFromAuthHeader_withWrongPrefix_returningToken() {
        final var authHeader = "Token " + TOKEN;
        assertEquals(StringUtils.EMPTY, TokenUtil.extractJwtFromAuthHeader(authHeader));
    }

    @Test
    void testExtractJwtFromAuthHeader_withNoPrefix_returningToken() {
        assertEquals(StringUtils.EMPTY, TokenUtil.extractJwtFromAuthHeader(TOKEN));
    }

    @Test
    void testGenerateSafeToken_returningToken() {
        final int tokenSize = 50;
        assertEquals(tokenSize, TokenUtil.generateSafeToken(tokenSize).length());
    }
}
