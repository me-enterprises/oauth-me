package ca.me.oauthme;

import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

/**
 * Abstract class encapsulating all the common features for all the Integration Tests.
 *
 * @author Rodrigo da Silva
 * @version 1.0.0
 * @since 1.0.0
 */
@SpringBootTest
@ExtendWith(SpringExtension.class)
public abstract class AbstractIntegrationTest extends AbstractTest {
}
