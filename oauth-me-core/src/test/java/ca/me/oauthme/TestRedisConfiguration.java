package ca.me.oauthme;

import org.springframework.boot.test.context.TestConfiguration;
import redis.embedded.RedisServer;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

/**
 * Test configuration for starting an embedded redis server.
 *
 * @author Rodrigo da Silva
 * @version 1.1.0
 * @since 1.1.0
 */
@TestConfiguration
public class TestRedisConfiguration {

    private final RedisServer redisServer;

    public TestRedisConfiguration() {
        this.redisServer = new RedisServer(26379);
    }

    @PostConstruct
    public void postConstruct() {
        redisServer.start();
    }

    @PreDestroy
    public void preDestroy() {
        redisServer.stop();
    }
}
