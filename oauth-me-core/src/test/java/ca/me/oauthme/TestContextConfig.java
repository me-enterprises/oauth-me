package ca.me.oauthme;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Class implemented to make it easier to implement tests in a modularized spring boot application.
 *
 * @author Rodrigo da Silva
 * @version 1.0.0
 * @since 1.0.0
 */
@SpringBootApplication
public class TestContextConfig {
    public static void main(String[] args) {
        SpringApplication.run(TestContextConfig.class, args);
    }
}
