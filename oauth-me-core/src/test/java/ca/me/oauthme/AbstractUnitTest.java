package ca.me.oauthme;

import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * Abstract class encapsulating all the common features for all the Unit Tests.
 *
 * @author Rodrigo da Silva
 * @version 1.0.0
 * @since 1.0.0
 */
@SpringBootTest
@ExtendWith(MockitoExtension.class)
public abstract class AbstractUnitTest extends AbstractTest {
}
