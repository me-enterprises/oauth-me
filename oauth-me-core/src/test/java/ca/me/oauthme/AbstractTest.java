package ca.me.oauthme;

import lombok.extern.slf4j.Slf4j;
import org.springframework.util.ReflectionUtils;

import java.util.Objects;

/**
 * Abstract class that encapsulates all common features between Unit and Integration Tests.
 *
 * @author Rodrigo da Silva
 * @version 1.0.0
 * @since 1.0.0
 */
@Slf4j
public abstract class AbstractTest {
    protected void setField(String name, Object value, Object target) {
        final var field = ReflectionUtils.findField(target.getClass(), name);
        if (Objects.nonNull(field)) {
            ReflectionUtils.makeAccessible(field);
            ReflectionUtils.setField(field, target, value);
        }
    }
}
