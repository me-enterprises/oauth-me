package ca.me.oauthme.repository;

import ca.me.oauthme.AbstractIntegrationTest;
import ca.me.oauthme.model.db.Scope;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.transaction.Transactional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Class implementing tests for the {@link ScopeRepository} repository.
 *
 * @author Rodrigo da Silva
 * @version 1.0.0
 * @since 1.0.0
 */
@Transactional
class ScopeRepositoryIntegrationTest extends AbstractIntegrationTest {
    private static final String SCOPE_1 = "Scope 1";

    @Autowired
    private ScopeRepository scopeRepository;

    @Test
    void testCreatedAtAndLastUpdatedAt_whenSavingAndUpdating() {
        var scope = createAndSaveScope();

        scope.setDescription("Scope Description 1_1");
        scope = scopeRepository.saveAndFlush(scope);
        assertNotNull(scope.getId(), "The ID field must not be NULL.");
        assertNotNull(scope.getAudit().getCreatedAt());
        assertNotNull(scope.getAudit().getLastUpdatedAt());
        assertEquals(SCOPE_1, scope.getName());
        assertEquals("Scope Description 1_1", scope.getDescription());
    }

    @Test
    void testFindByName() {
        var scopeOpt = scopeRepository.findByName(SCOPE_1);
        assertTrue(scopeOpt.isEmpty());
        final var scope = createAndSaveScope();
        scopeOpt = scopeRepository.findByName(SCOPE_1);
        assertTrue(scopeOpt.isPresent());
        assertEquals(scope.getName(), scopeOpt.orElse(new Scope()).getName());
        assertEquals(scope.getDescription(), scopeOpt.orElse(new Scope()).getDescription());
        assertEquals(scope.getId(), scopeOpt.orElse(new Scope()).getId());
        assertEquals(scope.getAudit(), scopeOpt.orElse(new Scope()).getAudit());
    }

    private Scope createAndSaveScope() {
        var scope = new Scope();
        scope.setName(SCOPE_1);
        scope.setDescription("Scope Description 1");
        scope = scopeRepository.saveAndFlush(scope);
        assertNotNull(scope.getId());
        assertNotNull(scope.getAudit().getCreatedAt());
        assertNull(scope.getAudit().getLastUpdatedAt());
        assertEquals(SCOPE_1, scope.getName());
        assertEquals("Scope Description 1", scope.getDescription());
        return scope;
    }
}
