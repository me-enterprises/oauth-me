package ca.me.oauthme.repository;

import ca.me.oauthme.AbstractIntegrationTest;
import ca.me.oauthme.model.db.RoleResourceScopeMapping;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.transaction.Transactional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Class implementing tests for the {@link RoleResourceScopeMappingRepository} repository.
 *
 * @author Rodrigo da Silva
 * @version 1.0.0
 * @since 1.0.0
 */
@Transactional
class RoleResourceScopeMappingRepositoryIntegrationTest extends AbstractIntegrationTest {

    @Autowired
    private RoleResourceScopeMappingRepository roleResourceScopeMappingRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private ResourceRepository resourceRepository;

    @Autowired
    private ScopeRepository scopeRepository;

    @Test
    void testFindByUser_whenUserIsNull() {
        assertTrue(roleResourceScopeMappingRepository.findByUser(null).isEmpty());
    }

    @Test
    void testFindByUser_whenUserDoesNotHaveRoles() {
        final var user = userRepository.findByUsername("no_role_user@email.com");
        assertTrue(user.isPresent());
        Set<RoleResourceScopeMapping> mapping = roleResourceScopeMappingRepository.findByUser(user.get());
        assertTrue(mapping.isEmpty());
    }

    @Test
    void testFindByUser_whenUserIsAdmin() {
        final var user = userRepository.findByUsername("admin@email.com");
        assertTrue(user.isPresent());
        final var role = roleRepository.findByName("ADMIN");
        assertTrue(role.isPresent());
        Set<RoleResourceScopeMapping> mapping = roleResourceScopeMappingRepository.findByUser(user.get());
        assertEquals(4, mapping.size());
        mapping.forEach(m -> assertEquals(role.get(), m.getId().getRole()));
    }
}
