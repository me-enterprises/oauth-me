package ca.me.oauthme.repository;

import ca.me.oauthme.model.db.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Interface containing all the methods available for querying a {@link User} from the database.
 *
 * @author Rodrigo da Silva
 * @version 1.0.0
 * @since 1.0.0
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    /**
     * Searches an {@link User} in the database by it's given unique username.
     *
     * @param username The username to be used in the search.
     * @return An {@link Optional} containing the {@link User} found, if any, or empty if none found.
     */
    Optional<User> findByUsername(String username);
}
