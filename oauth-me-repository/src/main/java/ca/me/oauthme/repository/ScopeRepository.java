package ca.me.oauthme.repository;

import ca.me.oauthme.model.db.Scope;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Interface containing all the methods available for querying a {@link Scope} from the database.
 *
 * @author Rodrigo da Silva
 * @version 1.0.0
 * @since 1.0.0
 */
@Repository
public interface ScopeRepository extends JpaRepository<Scope, Integer> {
    /**
     * Searches an {@link Scope} in the database by it's given unique name.
     *
     * @param name The name to be used in the search.
     * @return An {@link Optional} containing the {@link Scope} found, if any, or empty if none found.
     */
    Optional<Scope> findByName(String name);
}
