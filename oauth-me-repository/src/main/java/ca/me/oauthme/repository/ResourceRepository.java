package ca.me.oauthme.repository;

import ca.me.oauthme.model.db.Resource;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Interface containing all the methods available for querying a {@link Resource} from the database.
 *
 * @author Rodrigo da Silva
 * @version 1.0.0
 * @since 1.0.0
 */
@Repository
public interface ResourceRepository extends JpaRepository<Resource, Integer> {
}
