package ca.me.oauthme.repository;

import ca.me.oauthme.model.db.RoleResourceScopeMapping;
import ca.me.oauthme.model.db.User;
import ca.me.oauthme.model.db.pk.RoleResourceScopeMappingPk;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Set;

/**
 * Interface containing all the methods available for querying a {@link RoleResourceScopeMapping} from the database.
 *
 * @author Rodrigo da Silva
 * @version 1.0.0
 * @since 1.0.0
 */
@Repository
public interface RoleResourceScopeMappingRepository extends JpaRepository<RoleResourceScopeMapping, RoleResourceScopeMappingPk> {

    @Query(" from RoleResourceScopeMapping rrsm " +
            "join fetch rrsm.id.role   role " +
            "join fetch role.userRoles usro " +
            "where usro.id.user        = :user")
    Set<RoleResourceScopeMapping> findByUser(@Param("user") User user);
}
