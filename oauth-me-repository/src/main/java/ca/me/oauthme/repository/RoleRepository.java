package ca.me.oauthme.repository;

import ca.me.oauthme.model.db.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Interface containing all the methods available for querying a {@link Role} from the database.
 *
 * @author Rodrigo da Silva
 * @version 1.0.0
 * @since 1.0.0
 */
@Repository
public interface RoleRepository extends JpaRepository<Role, Integer> {
    /**
     * Searches a {@link Role} by its unique name.
     *
     * @param name The name to be searched
     * @return An optional containing the {@link Role} found or {@link Optional}.empty if none.
     */
    Optional<Role> findByName(String name);
}
