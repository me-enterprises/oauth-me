package ca.me.oauthme.controller;

import ca.me.oauthme.AbstractIntegrationTest;
import ca.me.oauthme.TestRedisConfiguration;
import ca.me.oauthme.controller.dto.ApiErrorResponse;
import ca.me.oauthme.controller.dto.ApiResponse;
import ca.me.oauthme.core.util.JwtUtil;
import ca.me.oauthme.service.dto.AuthenticationRequestDto;
import ca.me.oauthme.service.dto.AuthenticationResponseDto;
import ca.me.oauthme.service.dto.LoggedInResponseDto;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultMatcher;

import static ca.me.oauthme.controller.common.ApiConstants.OAUTH_IS_LOGGED_IN_BASE;
import static ca.me.oauthme.controller.common.ApiConstants.OAUTH_LOGIN_BASE;
import static ca.me.oauthme.controller.common.ApiConstants.OAUTH_LOGOUT_BASE;
import static ca.me.oauthme.core.ErrorCodes.E_OK;
import static ca.me.oauthme.core.ErrorCodes.E_PARAM_INVALID;
import static ca.me.oauthme.core.ErrorCodes.E_UNEXPECTED;
import static ca.me.oauthme.core.ErrorCodes.E_USER_INVALID_PASSWORD;
import static ca.me.oauthme.core.ErrorCodes.E_USER_NOT_FOUND;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Integration Test class for the {@link AuthenticationController} class
 *
 * @author Rodrigo da Silva
 * @version 1.0.0
 * @see AuthenticationController
 * @since 1.0.0
 */
@EnableCaching
@AutoConfigureMockMvc
@ContextConfiguration(classes = {TestRedisConfiguration.class})
class AuthenticationControllerIntegrationTest extends AbstractIntegrationTest {

    private static final String SUCCESS = "Success";
    private static final String USERNAME_ERROR = "The username cannot be null or empty!;";
    private static final String PASSWORD_ERROR = "The password cannot be null or empty!;";
    private static final String INVALID_USER_ERROR = "The user invalid@email.com could not be found on the system!";
    private static final String INVALID_PASSWORD_ERROR = "The password sent for the user user@email.com doesnt match the one found in the system!";
    private static final String UNEXPECTED_ERROR = "An unexpected error has occurred in the application! Error: Unexpected Error!.";
    private static final String USERNAME = "user@email.com";
    private static final String PASSWORD = "123456789";
    private static final String INVALID_USERNAME = "invalid@email.com";
    private static final String INVALID_PASSWORD = "invalid_password";
    private static final String IS_LOGGED_IN_SCOPE_READ = "isLoggedIn::read";

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper mapper;

    @SpyBean
    private JwtUtil jwtUtil;

    @Test
    void testDoLoginJson_whenNullUsernameAndPassword_returningParamInvalidError() throws Exception {
        final var request = AuthenticationRequestDto.builder().build();
        final MvcResult result = performLoginJson(request, status().isBadRequest());
        assertNotNull(result);
        final var response = mapper.readValue(result.getResponse().getContentAsString(), ApiErrorResponse.class);
        assertNotNull(response);
        assertNull(response.getData());
        assertEquals(E_PARAM_INVALID.code(), response.getStatusCode());
        assertTrue(response.getMessage().contains(USERNAME_ERROR));
        assertTrue(response.getMessage().contains(PASSWORD_ERROR));
        assertNotNull(response.getTimestamp());
        assertTrue(StringUtils.isNotBlank(response.getErrorDetail()));
    }

    @Test
    void testDoLoginJson_whenUserNotFound_returningUserNotFoundError() throws Exception {
        final var request = AuthenticationRequestDto.builder()
            .username(INVALID_USERNAME)
            .password(INVALID_PASSWORD)
            .build();
        final var result = performLoginJson(request, status().isBadRequest());
        assertNotNull(result);
        final var response = mapper.readValue(result.getResponse().getContentAsString(), ApiErrorResponse.class);
        assertNotNull(response);
        assertNull(response.getData());
        assertEquals(E_USER_NOT_FOUND.code(), response.getStatusCode());
        assertEquals(INVALID_USER_ERROR, response.getMessage());
        assertNotNull(response.getTimestamp());
        assertTrue(StringUtils.isNotBlank(response.getErrorDetail()));
    }

    @Test
    void testDoLoginJson_whenInvalidPassword_returningInvalidPasswordError() throws Exception {
        final var request = AuthenticationRequestDto.builder()
            .username(USERNAME)
            .password(INVALID_PASSWORD)
            .build();
        final var result = performLoginJson(request, status().isBadRequest());
        assertNotNull(result);
        final var response = mapper.readValue(result.getResponse().getContentAsString(), ApiErrorResponse.class);
        assertNotNull(response);
        assertNull(response.getData());
        assertEquals(E_USER_INVALID_PASSWORD.code(), response.getStatusCode());
        assertEquals(INVALID_PASSWORD_ERROR, response.getMessage());
        assertNotNull(response.getTimestamp());
        assertTrue(StringUtils.isNotBlank(response.getErrorDetail()));
    }

    @Test
    void testDoLoginJson_whenUnexpectedError_returningUnexpectedError() throws Exception {
        when(jwtUtil.generateToken(anyString(), anyString(), any()))
            .thenThrow(new RuntimeException("Unexpected Error!"));

        final var request = AuthenticationRequestDto.builder()
            .username(USERNAME)
            .password(PASSWORD)
            .build();

        final var result = performLoginJson(request, status().isInternalServerError());
        assertNotNull(result);
        final var response = mapper.readValue(result.getResponse().getContentAsString(), ApiErrorResponse.class);
        assertNotNull(response);
        assertNull(response.getData());
        assertEquals(E_UNEXPECTED.code(), response.getStatusCode());
        assertEquals(UNEXPECTED_ERROR, response.getMessage());
        assertNotNull(response.getTimestamp());
        assertTrue(StringUtils.isNotBlank(response.getErrorDetail()));
    }

    @Test
    void testDoLoginJson_returningSuccess() throws Exception {
        final var request = AuthenticationRequestDto.builder()
            .username(USERNAME)
            .password(PASSWORD)
            .build();

        final var result = performLoginJson(request, status().isOk());
        assertNotNull(result);
        final var response = mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<ApiResponse<AuthenticationResponseDto>>() {});
        assertNotNull(response);
        assertEquals(E_OK.code(), response.getStatusCode());
        assertEquals(SUCCESS, response.getMessage());
        assertNotNull(response.getData());
        assertTrue(StringUtils.isNotBlank(response.getData().getAccessToken()));
        final var scopeList = jwtUtil.getJwtScope(response.getData().getAccessToken());
        assertFalse(scopeList.isEmpty());
        assertTrue(scopeList.contains(IS_LOGGED_IN_SCOPE_READ));
    }

    @Test
    void testDoLoginFormUrlEncoded_returningSuccess() throws Exception {
        final var request = AuthenticationRequestDto.builder()
            .username(USERNAME)
            .password(PASSWORD)
            .build();

        final var result = performLoginFormUrlEncoded(request, status().isOk());
        assertNotNull(result);
        final var response = mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<ApiResponse<AuthenticationResponseDto>>() {});
        assertNotNull(response);
        assertEquals(E_OK.code(), response.getStatusCode());
        assertEquals(SUCCESS, response.getMessage());
        assertNotNull(response.getData());
        assertTrue(StringUtils.isNotBlank(response.getData().getAccessToken()));
        final var scopeList = jwtUtil.getJwtScope(response.getData().getAccessToken());
        assertFalse(scopeList.isEmpty());
        assertTrue(scopeList.contains(IS_LOGGED_IN_SCOPE_READ));
    }

    @Test
    void testIsLoggedIn_returningSuccess() throws Exception {
        final var request = AuthenticationRequestDto.builder()
            .username(USERNAME)
            .password(PASSWORD)
            .build();

        // Performs the login.
        var result = performLoginJson(request, status().isOk());
        final var authResponse = mapper.readValue(
            result.getResponse().getContentAsString(), new TypeReference<ApiResponse<AuthenticationResponseDto>>() {});
        final var token = authResponse.getData();

        // Checks if user is logged in.
        result = performIsLoggedIn(token.getAccessToken(), status().isOk());
        assertNotNull(result);
        final var loggedInResponse =
            mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<ApiResponse<LoggedInResponseDto>>() {});
        assertNotNull(loggedInResponse);
        assertNotNull(loggedInResponse.getData());
        assertTrue(loggedInResponse.getData().isLoggedIn());
    }

    @Test
    void testIsLoggedIn_whenUserNotLoggedIn_returningSuccess() throws Exception {
        final var request = AuthenticationRequestDto.builder()
            .username(USERNAME)
            .password(PASSWORD)
            .build();

        // Performs the login.
        var result = performLoginJson(request, status().isOk());
        final var authResponse = mapper.readValue(
            result.getResponse().getContentAsString(), new TypeReference<ApiResponse<AuthenticationResponseDto>>() {});
        final var token = authResponse.getData();

        // Performs the logout
        mockMvc
            .perform(post(OAUTH_LOGOUT_BASE + "/" + USERNAME))
            .andExpect(status().isAccepted())
            .andDo(print());

        // Checks if user is logged in.
        result = performIsLoggedIn(token.getAccessToken(), status().isOk());
        assertNotNull(result);
        final var loggedInResponse =
            mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<ApiResponse<LoggedInResponseDto>>() {});
        assertNotNull(loggedInResponse);
        assertNotNull(loggedInResponse.getData());
        assertFalse(loggedInResponse.getData().isLoggedIn());
    }

    @Test
    void testIsLoggedIn_whenInvalidJwt_returningForbidden() throws Exception {
        final var request = AuthenticationRequestDto.builder()
            .username(USERNAME)
            .password(PASSWORD)
            .build();

        // Performs the login.
        var result = performLoginJson(request, status().isOk());
        final var authResponse = mapper.readValue(
            result.getResponse().getContentAsString(), new TypeReference<ApiResponse<AuthenticationResponseDto>>() {});
        final var token = authResponse.getData();

        // Checks if user is logged in.
        doReturn(Boolean.FALSE)
            .when(jwtUtil).isJwtValid(eq(token.getAccessToken()), anyString(), anyString());
        performIsLoggedInReturningForbidden(token.getAccessToken());
    }

    @Test
    void testIsLoggedIn_withInvalidHeader_returningForbidden() throws Exception {
        performIsLoggedInReturningForbidden(INVALID_PASSWORD);
    }

    @Test
    void testIsLoggedIn_withWrongHeader_returningForbidden() throws Exception {
        mockMvc
            .perform(
                get(OAUTH_IS_LOGGED_IN_BASE)
                    .queryParam("username", USERNAME)
                    .header(HttpHeaders.AUTHORIZATION, INVALID_USERNAME))
            .andExpect(status().isForbidden())
            .andDo(print())
            .andReturn();
    }

    @Test
    void testIsLoggedIn_withNoHeader_returningForbidden() throws Exception {
        // Checks if user is logged in.
        mockMvc
            .perform(
                get(OAUTH_IS_LOGGED_IN_BASE))
            .andExpect(status().isForbidden())
            .andDo(print());
    }

    @Test
    void testLogout_returningAccepted() throws Exception {
        mockMvc
            .perform(post(OAUTH_LOGOUT_BASE + "/" + USERNAME))
            .andExpect(status().isAccepted())
            .andDo(print());
    }

    private MvcResult performLoginJson(AuthenticationRequestDto request, ResultMatcher result) throws Exception {
        return mockMvc
            .perform(
                post(OAUTH_LOGIN_BASE)
                    .accept(MediaType.APPLICATION_JSON)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(mapper.writeValueAsString(request)))
            .andExpect(result)
            .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
            .andDo(print())
            .andReturn();
    }

    private MvcResult performLoginFormUrlEncoded(AuthenticationRequestDto request, ResultMatcher result) throws Exception {
        return mockMvc
            .perform(
                post(OAUTH_LOGIN_BASE)
                    .accept(MediaType.APPLICATION_JSON)
                    .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                    .param("username", request.getUsername())
                    .param("password", request.getPassword()))
            .andExpect(result)
            .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
            .andDo(print())
            .andReturn();
    }

    private MvcResult performIsLoggedIn(String jwt, ResultMatcher result) throws Exception {
        return mockMvc
            .perform(
                get(OAUTH_IS_LOGGED_IN_BASE)
                    .queryParam("username", USERNAME)
                    .header(HttpHeaders.AUTHORIZATION, buildAuthorization(jwt)))
            .andExpect(result)
            .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
            .andDo(print())
            .andReturn();
    }

    private void performIsLoggedInReturningForbidden(String jwt) throws Exception {
        mockMvc
            .perform(
                get(OAUTH_IS_LOGGED_IN_BASE)
                    .queryParam("username", USERNAME)
                    .header(HttpHeaders.AUTHORIZATION, buildAuthorization(jwt)))
            .andExpect(status().isForbidden())
            .andDo(print())
            .andReturn();
    }

    private String buildAuthorization(String token) {
        return "Bearer " + token;
    }
}
