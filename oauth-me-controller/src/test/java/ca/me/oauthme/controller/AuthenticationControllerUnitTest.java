package ca.me.oauthme.controller;

import ca.me.oauthme.AbstractUnitTest;
import ca.me.oauthme.core.util.TokenUtil;
import ca.me.oauthme.service.AuthenticationService;
import ca.me.oauthme.service.dto.AuthenticationRequestDto;
import ca.me.oauthme.service.dto.AuthenticationResponseDto;
import ca.me.oauthme.service.exception.InvalidPasswordException;
import ca.me.oauthme.service.exception.TokenNotFoundException;
import ca.me.oauthme.service.exception.UnexpectedErrorException;
import ca.me.oauthme.service.exception.UserNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;

import java.util.Objects;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;

/**
 * Unit Test class for the {@link AuthenticationController} class
 *
 * @author Rodrigo da Silva
 * @version 1.0.0
 * @see AuthenticationController
 * @since 1.0.0
 */
class AuthenticationControllerUnitTest extends AbstractUnitTest {

    private static final String ACCESS_TOKEN = "ACCESS_TOKEN";
    private static final String REFRESH_TOKEN = "REFRESH_TOKEN";
    private static final String USERNAME = "USERNAME";
    private static final String SUCCESS = "Success";

    @Mock
    private AuthenticationService authService;

    @Mock
    private MessageSource messages;

    private AuthenticationController authController;
    private AuthenticationRequestDto authRequest;

    private final String authHeader = TokenUtil.AUTH_HEADER_PREFIX + "TOKEN";

    @BeforeEach
    void setUp() {
        if (Objects.isNull(authController)) {
            authController = new AuthenticationController(authService, messages);
        }

        authRequest = AuthenticationRequestDto.builder().build();
    }

    @Test
    void testDoLoginJson_whenUserNotFound_throwingUserNotFoundException()
        throws UserNotFoundException, UnexpectedErrorException, InvalidPasswordException {

        doThrow(new UserNotFoundException(messages, "USERNAME"))
            .when(authService).login(any(AuthenticationRequestDto.class));

        assertThrows(UserNotFoundException.class, () -> authController.doLoginJson(authRequest));
    }

    @Test
    void testDoLoginJson_whenInvalidPassword_throwingInvalidPasswordException()
        throws UserNotFoundException, UnexpectedErrorException, InvalidPasswordException {

        doThrow(new InvalidPasswordException(messages, "PASSWORD"))
            .when(authService).login(any(AuthenticationRequestDto.class));

        assertThrows(InvalidPasswordException.class, () -> authController.doLoginJson(authRequest));
    }

    @Test
    void testDoLoginJson_whenUnexpectedError_throwingUnexpectedErrorException()
        throws UserNotFoundException, UnexpectedErrorException, InvalidPasswordException {

        doThrow(new UnexpectedErrorException(messages, "Unexpected Error"))
            .when(authService).login(any(AuthenticationRequestDto.class));

        assertThrows(UnexpectedErrorException.class, () -> authController.doLoginJson(authRequest));
    }

    @Test
    void testDoLoginJson_whenSuccess_returningToken()
        throws UserNotFoundException, UnexpectedErrorException, InvalidPasswordException {

        doReturn(AuthenticationResponseDto.builder().accessToken(ACCESS_TOKEN).refreshToken(REFRESH_TOKEN).build())
            .when(authService).login(any(AuthenticationRequestDto.class));
        doReturn(SUCCESS)
            .when(messages).getMessage(anyString(), any(), any());

        final var response = authController.doLoginJson(authRequest);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        final var apiResponse = response.getBody();
        assertNotNull(apiResponse);
        assertEquals(0, apiResponse.getStatusCode());
        assertEquals(SUCCESS, apiResponse.getMessage());
        assertNotNull(apiResponse.getData());
        assertEquals(ACCESS_TOKEN, apiResponse.getData().getAccessToken());
    }

    @Test
    void testDoLoginFormUrlEncoded_whenSuccess_returningToken()
        throws UserNotFoundException, UnexpectedErrorException, InvalidPasswordException {

        doReturn(AuthenticationResponseDto.builder().accessToken(ACCESS_TOKEN).refreshToken(REFRESH_TOKEN).build())
            .when(authService).login(any(AuthenticationRequestDto.class));
        doReturn(SUCCESS)
            .when(messages).getMessage(anyString(), any(), any());

        final var response = authController.doLoginFormUrlEncoded(authRequest);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        final var apiResponse = response.getBody();
        assertNotNull(apiResponse);
        assertEquals(0, apiResponse.getStatusCode());
        assertEquals(SUCCESS, apiResponse.getMessage());
        assertNotNull(apiResponse.getData());
        assertEquals(ACCESS_TOKEN, apiResponse.getData().getAccessToken());
    }

    @Test
    void testIsLoggedIn_whenSuccess_returningTrue() throws TokenNotFoundException {
        doReturn(AuthenticationResponseDto.builder().accessToken(ACCESS_TOKEN).refreshToken(REFRESH_TOKEN).build())
            .when(authService).retrieveTokens(eq(USERNAME));
        doReturn(SUCCESS)
            .when(messages).getMessage(anyString(), any(), any());
        doReturn(USERNAME)
            .when(authService).getUsernameFromToken(anyString());

        final var response = authController.isLoggedIn(authHeader);
        assertNotNull(response);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        final var apiResponse = response.getBody();
        assertNotNull(apiResponse);
        assertEquals(0, apiResponse.getStatusCode());
        assertEquals(SUCCESS, apiResponse.getMessage());
        assertNotNull(apiResponse.getData());
        assertTrue(apiResponse.getData().isLoggedIn());
    }

    @Test
    void testIsLoggedIn_whenSuccess_returningFalse() throws TokenNotFoundException {
        doThrow(new TokenNotFoundException(messages, USERNAME))
            .when(authService).retrieveTokens(eq(USERNAME));
        doReturn(SUCCESS)
            .when(messages).getMessage(anyString(), any(), any());
        doReturn(USERNAME)
            .when(authService).getUsernameFromToken(anyString());

        final var response = authController.isLoggedIn(authHeader);
        assertNotNull(response);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        final var apiResponse = response.getBody();
        assertNotNull(apiResponse);
        assertEquals(0, apiResponse.getStatusCode());
        assertEquals(SUCCESS, apiResponse.getMessage());
        assertNotNull(apiResponse.getData());
        assertFalse(apiResponse.getData().isLoggedIn());
    }

    @Test
    void testLogout_whenAccepted() {
        doReturn(true)
            .when(authService).logout(eq(USERNAME));

        final var response = authController.logout(USERNAME);
        assertNotNull(response);
        assertEquals(HttpStatus.ACCEPTED, response.getStatusCode());
        assertNull(response.getBody());
    }
}
