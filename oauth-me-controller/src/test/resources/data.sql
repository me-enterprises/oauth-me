insert into tbme_scope(name, description, created_at)
values
    ('read', 'Read scope', current_timestamp);
select @scopeReadId  := id from tbme_scope where name = 'read';

insert into tbme_resource(path, name, description, created_at)
values
    ('/oauth/is-logged-in', 'isLoggedIn', 'Resource to indicate whether an user is currently logged in the application.', current_timestamp);
select @resourceIsLoggedInId := id from tbme_resource where name = 'isLoggedIn';

insert into tbme_role(name, description, created_at)
values
    ('ADMIN', 'Admin Role', current_timestamp),
    ('USER', 'User Role', current_timestamp);
select @roleAdminId := id from tbme_role where name = 'ADMIN';
select @roleUserId  := id from tbme_role where name = 'USER';

insert into tbme_user(username, password, unique_id, active, created_at)
values
    ('admin@email.com'       , '$2a$10$KH1hYEQ6hIruJ7dq4W2DuOYGqadr.CZUsdnteFxJPOYKhcBIDEO0y', 'c8afb570-dd5d-4c49-8bcd-0af9b47dc9d0', 1, current_timestamp),
    ('user@email.com'        , '$2a$10$KH1hYEQ6hIruJ7dq4W2DuOYGqadr.CZUsdnteFxJPOYKhcBIDEO0y', 'fc0d2e9e-f809-4ddd-8859-34fc8edcb984', 1, current_timestamp),
    ('no_role_user@email.com', '$2a$10$KH1hYEQ6hIruJ7dq4W2DuOYGqadr.CZUsdnteFxJPOYKhcBIDEO0y', '72dea1d7-3e83-4a87-a688-2e692750fce2', 1, current_timestamp);
select @userAdminId := id from tbme_user where username = 'admin@email.com';
select @userUserId  := id from tbme_user where username = 'user@email.com';

insert into tbme_user_role(user_id, role_id)
values
    (@userAdminId, @roleAdminId),
    (@userUserId, @roleUserId);

insert into tbme_role_resource_scope_mapping(role_id, resource_id, scope_id)
values
    (@roleAdminId, @resourceIsLoggedInId, @scopeReadId),
    (@roleUserId , @resourceIsLoggedInId, @scopeReadId);
