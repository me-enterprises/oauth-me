package ca.me.oauthme.controller.common;

/**
 * Class containing constants related to APIs.
 *
 * @author Rodrigo da Silva
 * @version 1.0.0
 * @since 1.0.0
 */
public abstract class ApiConstants {

    public static final String OAUTH_LOGIN_BASE = "/login";
    public static final String OAUTH_LOGOUT_BASE = "/logout";
    public static final String OAUTH_IS_LOGGED_IN_BASE = "/is-logged-in";
    private ApiConstants() {}
}
