package ca.me.oauthme.controller.advice;

import ca.me.oauthme.controller.dto.ApiErrorResponse;
import ca.me.oauthme.service.exception.InvalidPasswordException;
import ca.me.oauthme.service.exception.UnexpectedErrorException;
import ca.me.oauthme.service.exception.UserNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.Locale;

import static ca.me.oauthme.core.ErrorCodes.E_CREDENTIALS_INVALID;
import static ca.me.oauthme.core.ErrorCodes.E_PARAM_INVALID;

/**
 * Controller advice that will handle the various exception thrown by the application code.
 *
 * @author Rodrigo da Silva
 * @version 1.0.0
 * @since 1.0.0
 */
@RestControllerAdvice
public class OAuthControllerAdvice {

    private static final String ERR_SEPARATOR = "; ";

    private final MessageSource messages;

    @Autowired
    public OAuthControllerAdvice(MessageSource messages) {
        this.messages = messages;
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ApiErrorResponse handleValidationExceptions(MethodArgumentNotValidException ex) {
        final var builder = new StringBuilder();
        ex.getBindingResult().getAllErrors().forEach(error -> builder
            .append(error.getDefaultMessage())
            .append(ERR_SEPARATOR));
        return ApiErrorResponse.errorResponse(E_PARAM_INVALID, builder.toString(), ex.getMessage());
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(UserNotFoundException.class)
    public ApiErrorResponse handleUserNotFoundException(UserNotFoundException ex) {
        return ApiErrorResponse.errorResponse(ex);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(InvalidPasswordException.class)
    public ApiErrorResponse handleInvalidPasswordException(InvalidPasswordException ex) {
        return ApiErrorResponse.errorResponse(ex);
    }

    @ResponseStatus(HttpStatus.FORBIDDEN)
    @ExceptionHandler(AuthenticationCredentialsNotFoundException.class)
    public ApiErrorResponse handleAuthenticationCredentialsNotFoundException(AuthenticationCredentialsNotFoundException ex) {
        final var error = E_CREDENTIALS_INVALID;
        return ApiErrorResponse.errorResponse(
            error,
            messages.getMessage(error.property(), null, Locale.getDefault()),
            ex.getMessage());
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(UnexpectedErrorException.class)
    public ApiErrorResponse handleUnexpectedErrorException(UnexpectedErrorException ex) {
        return ApiErrorResponse.errorResponse(ex);
    }
}
