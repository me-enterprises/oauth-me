package ca.me.oauthme.controller;

import ca.me.oauthme.controller.dto.ApiResponse;
import ca.me.oauthme.core.util.SanitizerUtil;
import ca.me.oauthme.core.util.TokenUtil;
import ca.me.oauthme.service.AuthenticationService;
import ca.me.oauthme.service.dto.AuthenticationRequestDto;
import ca.me.oauthme.service.dto.AuthenticationResponseDto;
import ca.me.oauthme.service.dto.LoggedInResponseDto;
import ca.me.oauthme.service.exception.InvalidPasswordException;
import ca.me.oauthme.service.exception.TokenNotFoundException;
import ca.me.oauthme.service.exception.UnexpectedErrorException;
import ca.me.oauthme.service.exception.UserNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

import static ca.me.oauthme.controller.common.ApiConstants.OAUTH_IS_LOGGED_IN_BASE;
import static ca.me.oauthme.controller.common.ApiConstants.OAUTH_LOGIN_BASE;
import static ca.me.oauthme.controller.common.ApiConstants.OAUTH_LOGOUT_BASE;

/**
 * Controller class to handle OAuth2 authentication on the system.
 *
 * @author Rodrigo da Silva
 * @version 1.0.0
 * @since 1.0.0
 */
@Slf4j
@RestController
public class AuthenticationController {

    private static final String AUTHORITY_IS_LOGGED_IN_READ = "isLoggedIn::read";

    private final AuthenticationService authService;
    private final MessageSource messages;

    @Autowired
    public AuthenticationController(AuthenticationService authService, MessageSource messages) {
        this.authService = authService;
        this.messages = messages;
    }

    /**
     * Method that will be responsible by for performing the user login into the system. This method processes a JSON body
     *
     * @param authRequest The login request containing an username and a password.
     * @return The JWT token generated for the login.
     * @throws UserNotFoundException    If the user with the given username doesn't exist.
     * @throws InvalidPasswordException If the password sent doesn't match the one in the system.
     * @throws UnexpectedErrorException If any unexpected error occurs during the login.
     */
    @PostMapping(
        path = OAUTH_LOGIN_BASE,
        consumes = MediaType.APPLICATION_JSON_VALUE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ApiResponse<AuthenticationResponseDto>> doLoginJson(@Valid @RequestBody AuthenticationRequestDto authRequest)
        throws UserNotFoundException, InvalidPasswordException, UnexpectedErrorException {

        return doLogin(authRequest);
    }

    /**
     * Method that will be responsible by for performing the user login into the system. This method processes a form URL encoded
     *
     * @param authRequest The login request containing an username and a password.
     * @return The JWT token generated for the login.
     * @throws UserNotFoundException    If the user with the given username doesn't exist.
     * @throws InvalidPasswordException If the password sent doesn't match the one in the system.
     * @throws UnexpectedErrorException If any unexpected error occurs during the login.
     */
    @PostMapping(
        path = OAUTH_LOGIN_BASE,
        consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ApiResponse<AuthenticationResponseDto>> doLoginFormUrlEncoded(@Valid AuthenticationRequestDto authRequest)
        throws UserNotFoundException, InvalidPasswordException, UnexpectedErrorException {

        return doLogin(authRequest);
    }

    /**
     * Method that will be used to check if the user is still logged in the application.
     *
     * @return A response indicating whether the username is currently logged in the system.
     */
    @GetMapping(path = OAUTH_IS_LOGGED_IN_BASE, produces = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasAuthority('" + AUTHORITY_IS_LOGGED_IN_READ + "')")
    public ResponseEntity<ApiResponse<LoggedInResponseDto>> isLoggedIn(@RequestHeader(value = HttpHeaders.AUTHORIZATION, required = false) String authHeader) {
        authHeader = SanitizerUtil.sanitize(authHeader);
        String username = authService.getUsernameFromToken(TokenUtil.extractJwtFromAuthHeader(authHeader));
        log.info("##### :: Checking if USERNAME: {} is currently logged in the system.", username);
        try {
            return ResponseEntity.ok(
                ApiResponse.successResponse(
                    messages,
                    LoggedInResponseDto.builder()
                        .isLoggedIn(StringUtils.isNotBlank(authService.retrieveTokens(username).getAccessToken()))
                        .build()));
        } catch (TokenNotFoundException ex) {
            log.warn("##### :: AUTHENTICATION - User with USERNAME: {} not logged in!", username);
            return ResponseEntity.ok(
                ApiResponse.successResponse(
                    messages,
                    LoggedInResponseDto.builder()
                        .isLoggedIn(false)
                        .build()));
        }
    }

    /**
     * Logs out the user with the given username from the system.
     *
     * @param username The username to be logged out.
     * @return An HTTP 202 Accepted indicating the logout was done.
     */
    @PostMapping(path = OAUTH_LOGOUT_BASE + "/{username}")
    public ResponseEntity<Object> logout(@PathVariable("username") String username) {
        username = SanitizerUtil.sanitize(username);
        log.info("##### :: Logging out USERNAME: {} from the system.", username);
        authService.logout(username);
        log.info("##### :: USERNAME: {} successfully logged out.", username);
        return ResponseEntity.accepted().build();
    }

    private ResponseEntity<ApiResponse<AuthenticationResponseDto>> doLogin(AuthenticationRequestDto authRequest)
        throws UserNotFoundException, InvalidPasswordException, UnexpectedErrorException {

        log.info("##### :: AUTHENTICATION - Request BEFORE sanitization: {}", authRequest);
        authRequest = authRequest.sanitize();
        log.info("##### :: AUTHENTICATION - Request AFTER sanitization: {}", authRequest);
        return ResponseEntity.ok(ApiResponse.successResponse(messages, authService.login(authRequest)));
    }
}
