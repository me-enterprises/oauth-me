package ca.me.oauthme.controller.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;

import static ca.me.oauthme.controller.common.ApiConstants.OAUTH_IS_LOGGED_IN_BASE;
import static ca.me.oauthme.controller.common.ApiConstants.OAUTH_LOGIN_BASE;
import static ca.me.oauthme.controller.common.ApiConstants.OAUTH_LOGOUT_BASE;

/**
 * Class that will configure the security for the {@link ca.me.oauthme.controller.AuthenticationController} class
 *
 * @author Rodrigo da Silva
 * @version 1.0.0
 * @since 1.0.0
 */
@Slf4j
@Configuration
@Order(1)
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private static final String ANY = "/**";
    private static final String OAUTH_LOGIN = OAUTH_LOGIN_BASE;
    private static final String OAUTH_LOGOUT = OAUTH_LOGOUT_BASE + ANY;
    private static final String OAUTH_IS_LOGGED_IN = OAUTH_IS_LOGGED_IN_BASE + ANY;
    private static final String ACTUATOR = "/actuator";
    private static final String ACTUATOR_HEALTH = ACTUATOR + "/health";
    private static final String ACTUATOR_INFO = ACTUATOR + "/info";

    @Value("${actuator.security.enabled:true}")
    private boolean actuatorSecurityEnabled;

    @Value("${actuator.security.authority:ACTUATOR_ADMIN}")
    private String actuatorSecurityRole;

    // TODO study spring security for provinding authentication without the need of the AuthenticationFilter
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
            .authorizeRequests()
            .antMatchers(OAUTH_LOGIN, OAUTH_LOGOUT, OAUTH_IS_LOGGED_IN).permitAll()
            .and()
            .csrf().disable()
            .formLogin().disable()
            .logout().disable()
            .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        if (!actuatorSecurityEnabled) {
            http
                .authorizeRequests()
                .antMatchers(ACTUATOR + ANY).permitAll();
        } else {
            http
                .authorizeRequests()
                .antMatchers(ACTUATOR_HEALTH, ACTUATOR_INFO).permitAll()
                .antMatchers(ACTUATOR + ANY).hasAuthority(actuatorSecurityRole);
        }
    }
}
