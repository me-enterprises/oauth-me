package ca.me.oauthme.controller.dto;

import ca.me.oauthme.core.ErrorCodes;
import ca.me.oauthme.service.exception.BusinessException;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDateTime;
import java.time.ZoneOffset;

/**
 * Class that will be used to return an standardized error response to a client calling any controller.
 *
 * @author Rodrigo da Silva
 * @version 1.0.0
 * @since 1.0.0
 */
@Getter
@Setter
@ToString
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public final class ApiErrorResponse extends ApiResponse<Object> {
    private String errorDetail;
    private LocalDateTime timestamp;

    private ApiErrorResponse(int statusCode, String message, String errorDetail) {
        super(statusCode, message, null);
        this.errorDetail = errorDetail;
        this.timestamp = LocalDateTime.now(ZoneOffset.UTC);
    }

    /**
     * Creates an api error response for the given business exception.
     *
     * @param exception The business exception to be used on the error creation.
     * @return The api error response created.
     */
    public static ApiErrorResponse errorResponse(BusinessException exception) {
        return new ApiErrorResponse(
            exception.getCode(),
            exception.getMessage(),
            exception.getLocalizedMessage());
    }

    /**
     * Creates an api error response for the given status code, message, and error detail.
     *
     * @param err         The error code to be used.
     * @param message     The message to be used.
     * @param errorDetail The error detail to be used.
     * @return The api error response created.
     */
    public static ApiErrorResponse errorResponse(ErrorCodes err, String message, String errorDetail) {
        return new ApiErrorResponse(
            err.code(),
            message,
            errorDetail);
    }
}
