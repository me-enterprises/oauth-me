package ca.me.oauthme.controller.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.context.MessageSource;

import java.util.Locale;

import static ca.me.oauthme.core.ErrorCodes.E_OK;

/**
 * Class that will be used to return an standardized response to a client calling any controller.
 *
 * @param <T> The type that will be added to the data.
 * @author Rodrigo da Silva
 * @version 1.0.0
 * @since 1.0.0
 */
@Getter
@Setter
@ToString
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class ApiResponse<T> {
    protected final int statusCode;
    protected final String message;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    protected final T data;

    protected ApiResponse(int statusCode, String message, T data) {
        this.statusCode = statusCode;
        this.message = message;
        this.data = data;
    }

    /**
     * Creates a successful api response with the given message and data.
     *
     * @param messages The message source from where to get the message.
     * @param data     The data to be used.
     * @param <U>      The type of th data.
     * @return A successful api response.
     */
    public static <U> ApiResponse<U> successResponse(MessageSource messages, U data) {
        return new ApiResponse<>(
            E_OK.code(),
            messages.getMessage(E_OK.property(), null, Locale.getDefault()),
            data);
    }
}
