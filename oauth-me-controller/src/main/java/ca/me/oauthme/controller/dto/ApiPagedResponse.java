package ca.me.oauthme.controller.dto;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.context.MessageSource;

import java.util.List;
import java.util.Locale;

import static ca.me.oauthme.core.ErrorCodes.E_OK;

/**
 * Class that will be used to return an standardized paged response to a client calling any controller.
 *
 * @param <T> The type that will be added to the data.
 * @author Rodrigo da Silva
 * @version 1.0.0
 * @since 1.0.0
 */
@Getter
@Setter
@ToString(callSuper = true)
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public final class ApiPagedResponse<T> extends ApiResponse<List<T>> {
    private final int number;
    private final int size;
    private final int numberOfElements;

    private ApiPagedResponse(int statusCode, String message, List<T> data, int number, int size) {
        super(statusCode, message, data);
        this.number = number;
        this.size = size;
        this.numberOfElements = data.size();
    }

    /**
     * Creates a success api paged response with the given message, data, number, and size.
     *
     * @param messages The message source from where to get the message.
     * @param data     The data to be used.
     * @param number   The number of the page.
     * @param size     The size of the page
     * @param <U>      The type of the data.
     * @return A success api paged response.
     */
    public static <U> ApiPagedResponse<U> successResponse(MessageSource messages, List<U> data, int number, int size) {
        return new ApiPagedResponse<>(
            E_OK.code(),
            messages.getMessage(E_OK.property(), null, Locale.getDefault()),
            data,
            number,
            size);
    }
}
