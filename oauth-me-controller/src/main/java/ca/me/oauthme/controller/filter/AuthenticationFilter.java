package ca.me.oauthme.controller.filter;

import ca.me.oauthme.core.util.JwtUtil;
import ca.me.oauthme.repository.UserRepository;
import ca.me.oauthme.service.AuthenticationService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.stream.Collectors;

@Slf4j
@Component
public class AuthenticationFilter extends OncePerRequestFilter {

    private static final String AUTH_HEADER_PREFIX = "Bearer";

    private final AuthenticationService authService;
    private final UserRepository userRepository;
    private final JwtUtil jwtUtil;

    @Autowired
    public AuthenticationFilter(AuthenticationService authService, UserRepository userRepository, JwtUtil jwtUtil) {
        this.authService = authService;
        this.userRepository = userRepository;
        this.jwtUtil = jwtUtil;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request,
                                    HttpServletResponse response,
                                    FilterChain chain) throws ServletException, IOException {
        log.info("##### :: AUTH_FILTER - Requested URI: {}", request.getRequestURI());
        SecurityContextHolder.clearContext();
        final var authorization = request.getHeader(HttpHeaders.AUTHORIZATION);
        log.info("##### :: AUTH_FILTER - Found header AUTHORIZATION: {}", authorization);

        if (StringUtils.isNotBlank(authorization) && authorization.startsWith(AUTH_HEADER_PREFIX)) {
            final var jwt = extractToken(authorization);
            log.info("##### :: AUTH_FILTER - Extracted JWT: {}", jwt);
            final var username = jwtUtil.getJwtUsername(jwt);
            final var scopeList = jwtUtil.getJwtScope(jwt);
            log.info("##### :: AUTH_FILTER - JWT data USERNAME: {}, SCOPES: {}", username, scopeList);
            final var opDbUser = userRepository.findByUsername(username);

            if (opDbUser.isPresent()) {
                final var dbUser = opDbUser.get();
                if (jwtUtil.isJwtValid(jwt, dbUser.getUsername(), dbUser.getUniqueId())) {
                    final var grantedAuthorities = scopeList.stream().map(SimpleGrantedAuthority::new).collect(Collectors.toSet());
                    log.info("##### :: AUTH_FILTER - Setting USERNAME: {}, AUTHORITIES: {} on security context", username, grantedAuthorities);
                    SecurityContextHolder
                        .getContext()
                        .setAuthentication(new UsernamePasswordAuthenticationToken(username, dbUser.getPassword(), grantedAuthorities));
                } else {
                    log.warn("##### :: AUTH_FILTER - JWT: {} is no longer valid! Logging user out!", jwt);
                    authService.logout(username);
                }
            } else {
                log.warn("##### :: AUTH_FILTER - User with USERNAME: {} not found!", username);
            }
        } else {
            log.warn("##### :: AUTH_FILTER - Invalid AUTH_HEADER: {}", authorization);
        }

        chain.doFilter(request, response);
    }

    private String extractToken(String authorization) {
        final var tokenArr = authorization.split(" ");
        log.debug("##### :: AUTH_FILTER - Authorization TOKENS: {}", Arrays.asList(tokenArr));
        return tokenArr[tokenArr.length - 1];
    }
}
